<?php

namespace App\Mail;

use App\Models\Diary;
use App\Models\MailingList;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class DiaryCreatedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     */
    public function __construct(protected Diary $diary, protected MailingList $mailingList)
    {
        
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            subject: 'Mvonline: Nový výlet',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'mail/diaryCreated',
            with: [
                'url' => 'http://mvonline.eu',
                'page' => 'mvonline.eu',
                'frontend' => getenv("FRONTEND_URL") ?? "",
                'slug' => $this->diary->getSlug()->lower(),
                'title' => $this->diary->title,
                'image' => $this->diary->getHighlightImage(),
                'diaryLink' => url('',['diary'=>$this->diary->id]),
                'diaryId' => $this->diary->id,
                'hash' => route('remove_from_mailing', ['hash' => $this->mailingList->hash])
            ]
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
