<?php

namespace App\Methods;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use ZipArchive;

class ZipArchiver
{

    private Filesystem $fs;

    public function __construct(Filesystem $fs)
    {
        $this->fs = $fs;
    }

    public function extractZip($file, $extractTo): array
    {
        $zip = new \ZipArchive;

        try {
            if ($zip->open($file) === true) {
                $zip->extractTo($extractTo);
                $this->fs->delete($file);

                $files = array_filter(glob($extractTo.'/*'), 'is_file');
                return $files;
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
