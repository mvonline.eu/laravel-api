<?php

namespace App\Methods\DiaryFileUploader;

use App\Enums\UploadedFileTypes;
use App\Methods\FileUploader;
use App\Models\UploadedFile;
use App\Models\Diary;
use Illuminate\Http\UploadedFile as HttpUploadedFile;

class GPXFileUploader extends FileUploader implements DiaryFileUploader
{

    private $path = null;

    public function upload(HttpUploadedFile $file, Diary $diary)
    {
        $newFileFolder = 'storage/diary/' . $diary->id . '/';
        $newFileName = 'gpx.gpx';

        $path = $newFileFolder . $newFileName;

        $file->storeAs('uploads/diary/' . $diary->id, $file->getClientOriginalName(), 'public');

        $this->path = $path;
    }

    public function makeEntity(Diary $diary):UploadedFile|array
    {
        $uploadedFile = new UploadedFile([
            'path' => $this->path,
            'type' => UploadedFileTypes::GPX
        ]);

        $uploadedFile->save();

        return $uploadedFile;
    }
}
