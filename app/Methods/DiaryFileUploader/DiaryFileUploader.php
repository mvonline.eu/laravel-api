<?php

namespace App\Methods\DiaryFileUploader;

use App\Models\Diary;
use App\Models\UploadedFile;
use Illuminate\Http\UploadedFile as HttpUploadedFile;

interface DiaryFileUploader {

    public function upload(HttpUploadedFile $file, Diary $diary);

    public function makeEntity(Diary $diary): UploadedFile|array;

}