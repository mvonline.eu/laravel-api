<?php

namespace App\Methods\DiaryFileUploader;

use App\Enums\DiaryImage;
use App\Enums\UploadedFileTypes;
use App\Exceptions\AWSS3FileUploadException;
use App\Methods\AWSService;
use App\Methods\FileUploader;
use App\Methods\ZipArchiver;
use Illuminate\Http\UploadedFile;
use App\Models\Diary;
use App\Models\UploadedFile as ModelsUploadedFile;
use Illuminate\Filesystem\Filesystem;

class PhotogalleryFileUploader extends FileUploader implements DiaryFileUploader {

    private $diariesFiles = [];
    private ZipArchiver $zip;
    private AWSService $aws;
    private Filesystem $fs;

    public function __construct(ZipArchiver $zip, AWSService $aws, Filesystem $fs )
    {
       $this->zip = $zip;
       $this->aws = $aws;
       $this->fs = $fs;
    }

    /**
     * @throws AWSS3FileUploadException
     * @throws \Exception
     */
    public function upload(UploadedFile $file, Diary $diary): void
    {
        $path = 'storage/extractedDiaries';

        //zip
        $extracted = $this->zip->extractZip($file, $path);

        //aws
        $this->diariesFiles = $this->aws->uploadDiaryImages($extracted, $diary->id);

        //remove 'extractedDiaries'
        $this->fs->cleanDirectory($path);
    }

    public function makeEntity(Diary $diary): ModelsUploadedFile|array
    {
        $savedUploadedFiles = [];

        foreach ($this->diariesFiles as $diaryFile){

            $uploadedFile = new ModelsUploadedFile([
                'path' => $diaryFile[DiaryImage::IMAGE->value],
                'info' => $diaryFile,
                'type' => UploadedFileTypes::PHOTOGALLERY
            ]);

            $uploadedFile->save();
            $savedUploadedFiles[] = $uploadedFile;
        }

        return $savedUploadedFiles;
    }
}
