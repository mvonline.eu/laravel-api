<?php

namespace App\Methods;

use App\Models\Geocaching;

class GeocachingMethods {

    public function getLastNumber()
    {
        $geocache = Geocaching::orderBy('found', 'desc')->first();

        return $geocache?->number ?? 0;
    }

}