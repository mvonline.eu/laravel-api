<?php

namespace App\Methods;

use App\Exception\AWSS3FileUploadException;
use App\Exceptions\AWSS3FileUploadException as ExceptionsAWSS3FileUploadException;
use App\Models\App\AWSUploadedDiaryImage;
use Aws\Credentials\Credentials;
use Exception;

class AWSService {

    private $s3Client;

    public function __construct()
    {
        $credentials = new Credentials(config('services.aws.key'), config('services.aws.secret')); // Optional token

        $this->s3Client = new \Aws\S3\S3Client([
            'version' => 'latest',
            'region'  => config('services.aws.region'),
            'credentials' => $credentials,
        ]);
    }

    public function uploadDiaryImages(array $images, int $diaryID){
        $uploaded = [];
        try {
            foreach ($images as $image) {
                $baseName = basename($image);
                $fileName = substr($baseName, 0, strlen($baseName) - 4);
                $fileType = substr($baseName, strlen($baseName) - 3, strlen($baseName));
                $content = file_get_contents($image);

                $result = $this->s3Client->putObject([
                    'Bucket' => env("AWS_BUCKET"),
                    'Key' => 'diary' . '/' . $diaryID . '/' . $fileName . '.' . $fileType,
                    //'Key' => 'test' . '/' . $fileName . '.' . $fileType,
                    'SourceFile' => $image,
                    'ContentLength' => filesize($image),
                    'Body' => $content,
                    //fopen( $filePath, 'r' ),
                    'ACL' => 'public-read',
                ]);

                error_log("Result of uploading AWS image $result");

                $newFile = new AWSUploadedDiaryImage();
                $newFile->setImage('https://s3.us-east-2.amazonaws.com/mvonline.eu.images/diary/' . $diaryID . '/' . $fileName . '.' . $fileType);
                $newFile->setThumb('https://s3.us-east-2.amazonaws.com/mvonline.eu.images.resized/diary/' . $diaryID . '/' . $fileName . '_thumb.' . $fileType);
                $newFile->setLarge('https://s3.us-east-2.amazonaws.com/mvonline.eu.images.resized/diary/' . $diaryID . '/' . $fileName . '_1200.' . $fileType);
                $newFile->setMedium('https://s3.us-east-2.amazonaws.com/mvonline.eu.images.resized/diary/' . $diaryID . '/' . $fileName . '_600.' . $fileType);

                $uploaded[] = $newFile;
            }

            return array_map(function (AWSUploadedDiaryImage $item) {
                return $item->toArray();
            }, $uploaded);
        } catch (Exception $e) {
            throw new ExceptionsAWSS3FileUploadException($e);
        }

    }
}
