<?php

namespace App\Methods;

class FileMethods
{

    public function removeFiles($directory)
    {
        if (is_dir($directory)) {
            $files = glob($directory . '*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }
    }
}
