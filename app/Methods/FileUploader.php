<?php

namespace App\Methods;

use Illuminate\Http\UploadedFile;

abstract class FileUploader {

    public function uploadFile(UploadedFile $file, $to, string $fileName)
    {
        $file->move($to, $fileName);        
    }

}