<?php

namespace App\Listeners;

use App\Events\DiaryCreatedEvent;
use App\Mail\DiaryCreatedMail;
use App\Services\MailingListService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendDiaryCreatedMailMessagesListener
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     */
    public function __construct(private MailingListService $mailingListService)
    {
    }

    /**
     * Handle the event.
     */
    public function handle(DiaryCreatedEvent $event): void
    {
        $diary = $event->diary;

        $mailingList = $this->mailingListService->getAllMailingLists();
        foreach ($mailingList as $mailingListItem){
            Mail::to($mailingListItem->email)->send(new DiaryCreatedMail($diary, $mailingListItem));
        }
    }
}
