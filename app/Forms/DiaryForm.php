<?php

namespace App\Forms;

use App\Enums\FormMode;
use App\Forms\FormFields\DiaryTripTypeField;
use App\Forms\FormFields\PhotogalleryHighlight;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class DiaryForm extends Form
{

    public function buildForm(): void
    {
        $this->addCustomField('tripType', DiaryTripTypeField::class);
        $this->addCustomField('photogalleryHighlight', PhotogalleryHighlight::class);

        $this->add('title', Field::TEXT, [
            'rules' => 'required',
        ],
        )->add('keywords', Field::TEXT, [
            'rules' => 'required',
        ],
        )->add('trip_type', 'tripType',
        )->add('from_date', Field::DATE, [
            'rules' => 'required',
            'label' => 'Date',
            'format' => 'yyyy-MM-dd',
        ])->add('photos', Field::FILE, [

        ])->add('gpx', Field::FILE, [

        ]);

        if ($this->getFormOption('mode') === FormMode::MODE_EDIT) {
            $this->add('photogalleryHighlight', PhotogalleryHighlight::class);
        }

        $this->add('submit', Field::BUTTON_SUBMIT);
    }
}
