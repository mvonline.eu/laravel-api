<?php

namespace App\Forms\FormFields;

use Kris\LaravelFormBuilder\Fields\ChoiceType;

class DiaryTripTypeField extends ChoiceType
{
    protected function getTemplate(): string
    {
        return 'form.fields.diary-trip-type';
    }

    protected function getDefaults(): array
    {
        return array_merge(parent::getDefaults(), [
            'choices' => config('tripTypes'),
            'multiple' => true,
            'expanded' => true,
        ]);
    }

}
