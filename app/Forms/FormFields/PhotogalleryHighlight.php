<?php

namespace App\Forms\FormFields;

use Kris\LaravelFormBuilder\Fields\ChoiceType;

class PhotogalleryHighlight extends ChoiceType
{
    protected function getTemplate(): string
    {
        return 'form.fields.photogallery-highlight';
    }

    protected function getDefaults(): array
    {
        return array_merge(parent::getDefaults(), [
            'choices' => [],
            'multiple' => false,
            'expanded' => false,
        ]);
    }

    public function setChoices(array $choices): self
    {
        $this->setOption('choices', $choices);
        return $this;
    }
}
