<?php

namespace App\Strava;

use App\Common\Services\RestClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\InputBag;

class StravaRestClient extends RestClient
{
    const BASE_URL = 'https://www.strava.com';

    const AUTH_URL = self::BASE_URL . '/oauth/';
    const API_URL = self::BASE_URL . '/api/v3/';

    const ACCESS_TOKEN_MINIMUM_VALIDITY = 3600;

    private $clientId;
    private $clientSecret;
    private $accessToken;
    private $refreshToken;
    private $expiresAt;

    /**
     * @return mixed
     */
    public function __construct()
    {
        $this->clientId = config('strava.client_id');
        $this->clientSecret = config('strava.client_secret');
        $this->refreshToken = config('strava.refresh_token');
    }

    public function init(): void
    {
        $tokenResponse = $this->getAccessTokenResponse();
        $this->setAccessToken($tokenResponse['access_token'], $tokenResponse['refresh_token'], $tokenResponse['expires_at']);
    }

    public function getAccessTokenResponse()
    {
        if (! isset($this->refreshToken)){
            return null;
        }

        $request = new Request();

        $params = [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $this->refreshToken,
            'grant_type' => 'refresh_token',
        ];

        $request->setJson(new InputBag($params));
        $request->setMethod('POST');

        return $this->fetchContent(self::AUTH_URL . 'token', $request);
    }

    /**
     * @throws GuzzleException
     */
    public function getActivitiesList(int $page, int $perPage)
    {
        $request = new \Illuminate\Http\Request();
        $request->setMethod('GET');
        $request->query->set('page', $page);
        $request->query->set('perPage', $perPage);
        $request->query->set('access_token', $this->accessToken);

        return $this->doRequest(self::API_URL . 'athlete/activities', $request);
    }

    public function setAccessToken(string $token, ?string $refreshToken = null, ?string $expiresAt = null): void
    {
        if (isset($refreshToken)) {
            $this->refreshToken = $refreshToken;
        }

        if (isset($expiresAt)) {
            $this->expiresAt = $expiresAt;

            if ($this->isTokenRefreshNeeded()) {
                throw new \RuntimeException('Strava access token needs to be refreshed');
            }

            $this->accessToken = $token;
        }
    }

    public function isTokenRefreshNeeded(): bool
    {
        if (empty($this->expiresAt)){
            return false;
        }

        return $this->expiresAt - time() < self::ACCESS_TOKEN_MINIMUM_VALIDITY;
    }

    private function fetchContent(string $endpoint, \Illuminate\Http\Request $request)
    {
        try {
            return $this->doRequest($endpoint, $request);
        } catch (ConnectException | GuzzleException | ClientException $e) {
            print_r($e->getMessage());
        }
        return null;
    }

    public function doRequest($url, $request): mixed
    {
//        if ($request->method() === 'GET'){
//            $request->headers->set('Content-type', 'application/json');
//        }

        return parent::doRequest($url, $request);
    }

}
