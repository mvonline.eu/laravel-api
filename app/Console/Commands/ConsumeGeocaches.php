<?php

namespace App\Console\Commands;

use App\Methods\GeocachingMethods;
use App\Services\GeocachingProcessor;
use App\Services\RabbitMQService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ConsumeGeocaches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:consume-geocaches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private GeocachingProcessor $geocachingProcessor;
    private GeocachingMethods $geocachingMethods;

    public function __construct(GeocachingProcessor $geocachingProcessor, GeocachingMethods $geocachingMethods)
    {
        parent::__construct();
        $this->geocachingProcessor = $geocachingProcessor;
        $this->geocachingMethods = $geocachingMethods;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $rabbit = RabbitMQService::getConnection();
        $channel = RabbitMQService::getChannel();
        $channel->queue_declare('geocaches', false, false, false, false);

        while (true) {
            $callback = function ($msg) use ($channel) {
                $lastNumber = $this->geocachingMethods->getLastNumber();
                $cache = $msg->body;

                $cacheObj = json_decode($cache);

                $geocache = $this->geocachingProcessor->processWaypoint($cacheObj, $lastNumber);
                if ($geocache) {
                    $geocache->save();
                    $channel->basic_ack($msg->delivery_info['delivery_tag']);
                } else {
                    $channel->basic_reject($msg->delivery_info['delivery_tag'], false);
                }
            };

            $channel->basic_consume('geocaches', '', false, false, false, false, $callback);

            // Consume messages for a short period
            $channel->wait(null, false, 1);
        }

        $channel->close();
    }
}
