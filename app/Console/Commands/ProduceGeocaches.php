<?php

namespace App\Console\Commands;

use App\Services\GeocachingFileReader;
use App\Services\RabbitMQService;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\File as FacadesFile;

class ProduceGeocaches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:produce-geocaches';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    private GeocachingFileReader $gfr;

    public function __construct(GeocachingFileReader $gfr)
    {
        parent::__construct();
        $this->gfr = $gfr;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            $waypoints = $this->getWaypoints();
            RabbitMQService::publishMessages($waypoints,'geocaches', true);
            RabbitMQService::close();
        } catch(\Exception $ex){
            $this->error($ex->getMessage());
        }
    }

    private function getWaypoints(){
        $content = file_get_contents(env("GEOGET_FILE"));

        $this->gfr->setContent($content);
        $this->gfr->run();
        
        return $this->gfr->getGeocaches();
    }
}
