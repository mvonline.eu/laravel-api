<?php

namespace App\Console\Commands;

use App\Models\Diary;
use DateTimeInterface;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

//Todo remove after migration from old to new
class MigrateDiaries extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:migrate-diaries';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $oldDiaries = DB::select('SELECT * FROM bp_diary ORDER BY id');

        foreach ($oldDiaries as $oldDiary) {
            $date = new \DateTime();
            $id = $oldDiary->id;
            $title = $oldDiary->title;
            $from_date = $oldDiary->from_date;
            $to_date = $oldDiary->to_date;
            $keywords = $oldDiary->keywords;
            $highlight = (int) $oldDiary->highlight;
            $query = "INSERT INTO diaries SET id='$id', title='$title', from_date='$from_date', ";
            if ($to_date) {
                $query .= "to_date='$to_date',";
            }
            $query .= "keywords='$keywords', highlight='$highlight'";

            $photogallery = unserialize($oldDiary->photogallery);

            DB::insert($query);

            foreach ($photogallery as $file) {
                $diaryId = $oldDiary->id;
                $type = 'photo';
                $info = json_encode($file);
                $path = is_array($file) ? $file['image'] : $file->image;
                DB::insert("INSERT INTO uploaded_file SET path='$path', type='$type', info='$info'");
                $uploadedFileId = DB::select("SELECT LAST_INSERT_ID() as id;");
                $uploadedFileId = (int)$uploadedFileId[0]->id;

                //insert into diaries_uploaded_file
                DB::insert("INSERT INTO diary_uploaded_file SET diary_id='$diaryId', uploaded_file_id='$uploadedFileId'");
            }

            $gpx = $oldDiary->gpx;
            if ($gpx){
                DB::insert("INSERT INTO uploaded_file SET path='$gpx', type='gpx'");
                $uploadedFileId = DB::select("SELECT LAST_INSERT_ID() as id;");
                $uploadedFileId = (int)$uploadedFileId[0]->id;
                DB::insert("INSERT INTO diary_uploaded_file SET diary_id='$diaryId', uploaded_file_id='$uploadedFileId'");
            }

            $tripTypes = DB::select('SELECT * FROM bp_diaries_triptypes WHERE diary_id ='.$diaryId);
            foreach ($tripTypes as $tripType) {
                DB::insert('INSERT INTO diary_trip_type SET diary_id='.$tripType->diary_id.', trip_type_id='.$tripType->trip_type_id);
            }
        }
    }
}
