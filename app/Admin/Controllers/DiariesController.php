<?php

namespace App\Admin\Controllers;

use App\Enums\FormMode;
use App\Factories\DiaryFactory;
use App\Forms\DiaryForm;
use App\Methods\DiaryFileUploader\GPXFileUploader;
use App\Methods\DiaryFileUploader\PhotogalleryFileUploader;
use App\Models\Diary;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Kris\LaravelFormBuilder\FormBuilder;

class DiariesController
{
    public function list(): View
    {
        /** @var LengthAwarePaginator $diaries */
        $diaries = Diary::latest('id')->paginate(15);

        return view('admin/diaries/list', [
            'diaries' => $diaries,
        ]);
    }

    public function add(FormBuilder $formBuilder): View
    {
        return $this->renderForm($formBuilder, FormMode::MODE_ADD);
    }

    /**
     * @throws \Exception
     */
    public function postDiary(FormBuilder $formBuilder, Request $request): RedirectResponse
    {
        $form = $formBuilder->create(DiaryForm::class);

        if (!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        try {
            $values = array_merge($form->getFieldValues(), $request->all());
            $diary = DiaryFactory::fromFieldValues($values);
            $diary->save();

            $this->handleFileUploads($values, $diary);

            $message = $request->get('mode') === FormMode::MODE_EDIT->value
                ? 'Diary has been updated'
                : 'Diary has been created';

            return redirect()->route('admin.diaries')->with('success', $message);
        } catch (\Exception $exception) {
            return redirect()->route('admin.diaries')->with('error',
                'Diary has not been created due to an exception: ' . $exception->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    public function edit(int $id, FormBuilder $formBuilder): View | RedirectResponse
    {
        $diary = Diary::with(['tripTypes', 'uploadedFiles'])->find($id);

        if (!$diary) {
            return redirect()->route('admin.diaries');
        }

        return $this->renderForm($formBuilder, FormMode::MODE_EDIT, $diary);
    }

    private function renderForm(FormBuilder $formBuilder, FormMode $mode, Diary $diary = null): View
    {
        $options = ['mode' => $mode, 'model' => $diary];
        $diaryForm = $formBuilder->create(DiaryForm::class, $options, $diary?->toArray() ?? []);

        $diaryForm->add('mode', 'hidden', ['value' => $mode->value])
            ->setMethod('POST')
            ->setUrl(route('admin.diaries.postDiary'));

        if ($mode === FormMode::MODE_EDIT && $diary) {
            $diaryForm->add('id', 'hidden');
            $diaryForm->getField('trip_type')->setOption('value', $diary->tripTypes->pluck('id')->toArray());

            $photos = $diary->uploadedFiles()->where('type', 'photo')->get();
            $diaryForm->getField('photogalleryHighlight')
                ->setChoices($photos->toArray())
                ->setOption('value', $diary->highlight);
        }

        return view('admin/diaries/form', [
            'form' => $diaryForm,
            'mode' => $mode,
        ]);
    }

    private function handleFileUploads(array $values, Diary $diary): void
    {
        if (!empty($values['gpx'])) {
            app(GPXFileUploader::class)->upload($values['gpx'], $diary);
        }

        if (!empty($values['photos'])) {
            app(PhotogalleryFileUploader::class)->upload($values['photos'], $diary);
        }
    }
}
