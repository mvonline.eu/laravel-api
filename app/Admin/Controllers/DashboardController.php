<?php

namespace App\Admin\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class DashboardController
{
    public function index(): RedirectResponse
    {
        return redirect()->route('admin.dashboard');
    }

    public function dashboard(): View
    {
        $administrationLinks = config('admin.links');
        return view('admin/dashboard', [
            'links' => $administrationLinks,
        ]);
    }
}
