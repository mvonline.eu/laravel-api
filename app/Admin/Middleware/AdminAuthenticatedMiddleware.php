<?php

namespace App\Admin\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuthenticatedMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $user = Auth::user();

        if(!$user && !($user?->isAdmin() ?? false)){
            return redirect()->route('admin.login');
        }

        return $next($request);
    }

}
