<?php

namespace App\Models\DTO;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     schema="DiariesListQuery",
 *     type="object",
 *     description="Query parameters for listing diaries.",
 *     @OA\Property(
 *         property="offset",
 *         type="integer",
 *         description="The number of items to skip.",
 *         example=0
 *     ),
 *     @OA\Property(
 *         property="limit",
 *         type="integer",
 *         description="The maximum number of items to return.",
 *         example=10
 *     ),
 *     @OA\Property(
 *         property="orderBy",
 *         type="string",
 *         description="Field to order the diaries by.",
 *         example="from_date"
 *     ),
 *     @OA\Property(
 *         property="direction",
 *         type="string",
 *         description="Sort direction (asc or desc).",
 *         enum={"asc", "desc"},
 *         example="asc"
 *     ),
 *     @OA\Property(
 *         property="withPhotos",
 *         type="boolean",
 *         description="Inject photos to result",
 *         example=true
 *     )
 * )
 */
class DiariesListQueryDTO
{
    public ?int $offset;
    public ?int $limit;
    public ?string $orderBy;
    public ?string $direction;
    public ?bool $withPhotos;

    public function __construct(array $data)
    {
        $this->offset = $data['offset'] ?? null;
        $this->limit = $data['limit'] ?? null;
        $this->orderBy = $data['orderBy'] ?? null;
        $this->direction = $data['direction'] ?? null;
        $this->withPhotos = $data['withPhotos'] ?? null;
    }
}
