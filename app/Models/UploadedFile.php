<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UploadedFile extends Model
{
    use HasFactory;

    protected $table = "uploaded_file";

    protected $hidden = [
        'pivot'
    ];

    protected $fillable = [
        'path', 'type', 'info'
    ];

    protected $casts = [
        "info" => "array"
    ];

    public function diary()
    {
        return $this->belongsTo(Diary::class);
    }
}
