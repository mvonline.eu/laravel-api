<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeocachingCountry extends Model
{
    use HasFactory;

    public $table = 'geocaching_country';
}
