<?php

namespace App\Models\App;

use App\Enums\DiaryImage;

class AWSUploadedDiaryImage
{
    private $image;
    private $medium;
    private $thumb;
    private $large;

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getLarge(): ?string
    {
        return $this->large;
    }

    public function setLarge(string $large): self
    {
        $this->large = $large;

        return $this;
    }

    public function getMedium(): ?string
    {
        return $this->medium;
    }

    public function setMedium(string $medium): self
    {
        $this->medium = $medium;

        return $this;
    }

    public function getThumb(): ?string
    {
        return $this->thumb;
    }

    public function setThumb(string $thumb): self
    {
        $this->thumb = $thumb;

        return $this;
    }

    public function toArray()
    {
        return [
            DiaryImage::IMAGE->value => $this->getImage(),
            DiaryImage::LARGE->value => $this->getLarge(),
            DiaryImage::MEDIUM->value => $this->getMedium(),
            DiaryImage::THUMB->value => $this->getThumb()
        ];
    }
}
