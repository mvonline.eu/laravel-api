<?php

namespace App\Models\API\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     schema="DiaryGeoDataResponse",
 *     type="object",
 *     @OA\Property(property="start", ref="#/components/schemas/NominatimLocationData"),
 *     @OA\Property(property="end", ref="#/components/schemas/NominatimLocationData"),
 *     @OA\Property(property="polylines", ref="#/components/schemas/GPXPolyline"),
 *     @OA\Property(property="distance", type="number"),
 *     @OA\Property(property="avgSpeed", type="number"),
 *     @OA\Property(property="minAltitude", type="integer"),
 *     @OA\Property(property="maxAltitude", type="integer"),
 *     @OA\Property(property="elevationGain", type="integer"),
 *     @OA\Property(property="duration", type="number")
 * )
 */
class DiaryGeoDataResponse
{
    private NominatimLocationDataResponse $start;
    private NominatimLocationDataResponse $end;

    private float $distance;
    private float $avgSpeed;
    private int $minAltitude;
    private int $maxAltitude;
    private int $elevationGain;
    private float $duration;
    private array $polylines;

    public static function fromStats(array $stats): self
    {
        $instance = new self();

        foreach ($stats as $key => $statItem) {
            $method = 'set' . ucfirst($key);
            if (method_exists($instance, $method)) {
                $instance->$method($statItem);
            }
        }

        return $instance;
    }

    public function setStart(NominatimLocationDataResponse $start): DiaryGeoDataResponse
    {
        $this->start = $start;
        return $this;
    }

    public function setEnd(NominatimLocationDataResponse $end): DiaryGeoDataResponse
    {
        $this->end = $end;
        return $this;
    }

    public function setDistance(float $distance): DiaryGeoDataResponse
    {
        $this->distance = round($distance / 1000, 2);
        return $this;
    }

    public function setAvgSpeed(float $avgSpeed): DiaryGeoDataResponse
    {
        $this->avgSpeed = round($avgSpeed * 3.6, 2);
        return $this;
    }

    public function setMinAltitude(int $minAltitude): DiaryGeoDataResponse
    {
        $this->minAltitude = $minAltitude;
        return $this;
    }

    public function setMaxAltitude(int $maxAltitude): DiaryGeoDataResponse
    {
        $this->maxAltitude = $maxAltitude;
        return $this;
    }

    public function setCumulativeElevationGain(int $elevationGain): DiaryGeoDataResponse
    {
        $this->elevationGain = $elevationGain;
        return $this;
    }

    public function setDuration(float $duration): DiaryGeoDataResponse
    {
        //set duration in minutes
        $this->duration = $duration / 60;
        return $this;
    }

    public function setPolylines(array $polylines): DiaryGeoDataResponse
    {
        $this->polylines = $polylines;
        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'start' => $this->start->jsonSerialize(),
            'end' => $this->end->jsonSerialize(),
            'distance' => $this->distance,
            'avgSpeed' => $this->avgSpeed,
            'minAltitude' => $this->minAltitude,
            'maxAltitude' => $this->maxAltitude,
            'elevationGain' => $this->elevationGain,
            'duration' => $this->duration,
            'polylines' => $this->polylines,
        ];
    }
}
