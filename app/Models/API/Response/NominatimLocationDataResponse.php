<?php

namespace App\Models\API\Response;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     schema="NominatimLocationData",
 *     type="object",
 *     @OA\Property(property="house_number", type="string", nullable=true),
 *     @OA\Property(property="road", type="string", nullable=true),
 *     @OA\Property(property="neighbourhood", type="string", nullable=true),
 *     @OA\Property(property="suburb", type="string", nullable=true),
 *     @OA\Property(property="village", type="string", nullable=true),
 *     @OA\Property(property="town", type="string", nullable=true),
 *     @OA\Property(property="city", type="string", nullable=true),
 *     @OA\Property(property="municipality", type="string", nullable=true),
 *     @OA\Property(property="county", type="string", nullable=true),
 *     @OA\Property(property="state", type="string", nullable=true),
 *     @OA\Property(property="ISO3166-2-lvl4", type="string", nullable=true),
 *     @OA\Property(property="postcode", type="string", nullable=true),
 *     @OA\Property(property="country", type="string", nullable=true),
 *     @OA\Property(property="country_code", type="string", nullable=true),
 *     @OA\Property(property="continent", type="string", nullable=true),
 *     @OA\Property(property="state_district", type="string", nullable=true),
 *     @OA\Property(property="region", type="string", nullable=true),
 *     @OA\Property(property="borough", type="string", nullable=true),
 *     @OA\Property(property="hamlet", type="string", nullable=true),
 *     @OA\Property(property="croft", type="string", nullable=true),
 *     @OA\Property(property="residential", type="string", nullable=true)
 * )
 */
class NominatimLocationDataResponse implements \JsonSerializable
{
    private ?string $road;
    private ?string $suburb;
    private ?string $village;
    private ?string $town;
    private ?string $city;
    private ?string $county;
    private ?string $state;
    private ?string $iso31662lvl4;
    private ?string $postcode;
    private ?string $country;
    private ?string $countryCode;
    private ?string $region;

    public static function createFromLocation(array $locationData): self
    {
        $response = new self();
        $response
            ->setRoad($locationData['road'] ?? null)
            ->setSuburb($locationData['suburb'] ?? null)
            ->setVillage($locationData['village'] ?? null)
            ->setTown($locationData['town'] ?? null)
            ->setCity($locationData['city'] ?? null)
            ->setCounty($locationData['county'] ?? null)
            ->setState($locationData['state'] ?? null)
            ->setIso31662lvl4($locationData['ISO3166-2-lvl4'] ?? null)
            ->setPostcode($locationData['postcode'] ?? null)
            ->setCountry($locationData['country'] ?? null)
            ->setCountryCode($locationData['country_code'] ?? null)
            ->setRegion($locationData['region'] ?? null);

        return $response;
    }

    public function setRoad(?string $road): NominatimLocationDataResponse
    {
        $this->road = $road;
        return $this;
    }

    public function setSuburb(?string $suburb): NominatimLocationDataResponse
    {
        $this->suburb = $suburb;
        return $this;
    }

    public function setVillage(?string $village): NominatimLocationDataResponse
    {
        $this->village = $village;
        return $this;
    }

    public function setTown(?string $town): NominatimLocationDataResponse
    {
        $this->town = $town;
        return $this;
    }

    public function setCity(?string $city): NominatimLocationDataResponse
    {
        $this->city = $city;
        return $this;
    }

    public function setCounty(?string $county): NominatimLocationDataResponse
    {
        $this->county = $county;
        return $this;
    }

    public function setState(?string $state): NominatimLocationDataResponse
    {
        $this->state = $state;
        return $this;
    }

    public function setIso31662lvl4(?string $iso31662lvl4): NominatimLocationDataResponse
    {
        $this->iso31662lvl4 = $iso31662lvl4;
        return $this;
    }

    public function setPostcode(?string $postcode): NominatimLocationDataResponse
    {
        $this->postcode = $postcode;
        return $this;
    }

    public function setCountry(?string $country): NominatimLocationDataResponse
    {
        $this->country = $country;
        return $this;
    }

    public function setCountryCode(?string $countryCode): NominatimLocationDataResponse
    {
        $this->countryCode = $countryCode;
        return $this;
    }

    public function setRegion(?string $region): NominatimLocationDataResponse
    {
        $this->region = $region;
        return $this;
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'road' => $this->road,
            'suburb' => $this->suburb,
            'village' => $this->village,
            'town' => $this->town,
            'city' => $this->city,
            'county' => $this->county,
            'state' => $this->state,
            'iso31662lvl4' => $this->iso31662lvl4,
            'postcode' => $this->postcode,
            'country' => $this->country,
            'country_code' => $this->countryCode,
            'region' => $this->region,
        ], fn($value) => $value !== null);
    }
}
