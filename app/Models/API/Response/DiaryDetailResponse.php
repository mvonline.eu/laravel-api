<?php

namespace App\Models\API\Response;

use App\Models\Diary;
use DateTimeInterface;
use JsonSerializable;

class DiaryDetailResponse implements JsonSerializable
{
    private int $id;
    private string $title;
    private string $slug;
    private \DateTimeImmutable $date;
    private array $tripTypes;
    private array $photogallery;

    /**
     * @throws \Exception
     */
    public static function fromDiary(Diary $diary): DiaryDetailResponse
    {
        $diaryResponse = new DiaryDetailResponse();
        $diaryResponse
            ->setId($diary->id)
            ->setDate(new \DateTimeImmutable($diary->from_date))
            ->setTitle($diary->title)
            ->setSlug($diary->slug)
        ;

        $tripTypes = $diary->tripTypes->unique();
        $tripTypesMap = $tripTypes->map(function ($tripType) {;
            return new DiaryTripTypesResponse($tripType);
        });

        $diaryResponse->setTripTypes($tripTypesMap->toArray());

        $photogallery = $diary->getPhotogallery();

        $photogalleryMap = $photogallery->map(function ($photo){
            return $photo->info;
        });

        $diaryResponse->setPhotogallery($photogalleryMap->toArray());

        return $diaryResponse;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): DiaryDetailResponse
    {
        $this->title = $title;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): DiaryDetailResponse
    {
        $this->id = $id;
        return $this;
    }

    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): DiaryDetailResponse
    {
        $this->date = $date;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): DiaryDetailResponse
    {
        $this->slug = $slug;
        return $this;
    }

    /** @return DiaryTripTypesResponse[] */
    public function getTripTypes(): array
    {
        return $this->tripTypes;
    }

    /**
     * @param DiaryTripTypesResponse[] $tripTypes
     */
    public function setTripTypes(array $tripTypes): DiaryDetailResponse
    {
        $this->tripTypes = $tripTypes;
        return $this;
    }

    public function getPhotogallery(): array
    {
        return $this->photogallery;
    }

    public function setPhotogallery(array $photogallery): DiaryDetailResponse
    {
        $this->photogallery = $photogallery;
        return $this;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'date' => $this->date->format(DateTimeInterface::ATOM),
            'slug' => $this->slug,
            'tripTypes' => $this->tripTypes,
            'photogallery' => $this->photogallery,
        ];
    }
}
