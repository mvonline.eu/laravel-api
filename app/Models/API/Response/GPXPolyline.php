<?php

namespace App\Models\API\Response;

use OpenApi\Annotations as OA;

class GPXPolyline implements \JsonSerializable
{
    private float $longitude;
    private float $latitude;
    private float $elevation;
    private ?float $distance;

    /**
     * @OA\Schema(
     *     schema="GPXPolyline",
     *     type="object",
     *     @OA\Property(property="longitude", type="float"),
     *     @OA\Property(property="latitude", type="float"),
     *     @OA\Property(property="elevation", type="float"),
     *     @OA\Property(property="distance", type="float", nullable="true"),
     * )
     */
    public function __construct(float $longitude, float $latitude, float $elevation, ?float $distance)
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
        $this->elevation = $elevation;
        $this->distance = $distance;
    }

    public function jsonSerialize(): array
    {
        return [
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'elevation' => $this->elevation,
            'distance' => $this->distance,
        ];
    }
}
