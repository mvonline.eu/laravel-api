<?php

namespace App\Models\API\Response;

use App\Models\TripType;

class DiaryTripTypesResponse implements \JsonSerializable
{
    public function __construct(TripType $tripType)
    {
        $this->id = $tripType->id;
        $this->name = $tripType->text;
        $this->icon = $tripType->icon;
    }

    private int $id;
    private string $name;
    private string $icon;

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name,
            'icon' => $this->icon,
        ];
    }
}
