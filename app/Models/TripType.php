<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TripType extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    public function diaries(): BelongsToMany
    {
        return $this->belongsToMany(Diary::class);
    }
}
