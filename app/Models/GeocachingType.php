<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeocachingType extends Model
{
    use HasFactory;

    protected $table = 'geocaching_type';
}
