<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Geocaching extends Model
{
    use HasFactory;

    protected $table = "geocaching";

    public $timestamps = false;

    protected $guarded = [];

    public function type()
    {
        return $this->belongsTo(GeocachingType::class, 'type');
    }

    public function country()
    {
        return $this->belongsTo(GeocachingCountry::class, 'country');
    }
}
