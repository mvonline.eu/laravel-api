<?php

namespace App\Models;

use App\Casts\DateFormatCast;
use App\Enums\UploadedFileTypes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Symfony\Component\String\AbstractUnicodeString;
use Symfony\Component\String\Slugger\AsciiSlugger;

class Diary extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'keywords',
        'from_date',
    ];

    protected $casts = [
        'from_date' => DateFormatCast::class,
        'type' => 'array',
    ];

    protected $hidden = ['pivot', 'created_at', 'updated_at', 'to_date', 'highlight', 'keywords', 'uploadedFiles'];

    protected $appends = ['image', 'slug'];

    public function getImageAttribute()
    {
        $highlight = $this->highlight ?? 0;

        $uploadedPhotos = $this->uploadedFiles()->where('type', '=', 'photo')->get();

        if ($uploadedPhotos->count() === 0) {
            return null;
        }

        $highlightImage = $uploadedPhotos->offsetGet($highlight);

        return $highlightImage->info['medium'] ?? $highlightImage->info['image'];
    }

    public function uploadedFiles()
    {
        return $this->belongsToMany(UploadedFile::class);
    }

    public function getGpx()
    {
        return $this->uploadedFiles()->where('type', UploadedFileTypes::GPX)->get();
    }

    public function getPhotogallery()
    {
        return $this->uploadedFiles()->where('type', UploadedFileTypes::PHOTOGALLERY)->get();
    }

    public function getPhotos(): array
    {
        $photogallery = $this->getPhotogallery();

        $toRet = $photogallery->map(function ($uploadedFile) {
            return $uploadedFile->info;
        });

        return $toRet->toArray();
    }

    public function tripTypes(): BelongsToMany
    {
        return $this->belongsToMany(TripType::class);
    }

    public function getHighlightImage()
    {
        $file = $this->photogallery->get($this->highlight ?? 0);
        if ($file instanceof UploadedFile) {
            $gallery = $file->info;
            return $gallery;
        }
    }

    public function getFormattedDate(): string
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->from_date);
        return $date->format('d.m.Y');
    }

    public function getSlugAttribute(): string
    {
        $slugger = new AsciiSlugger();

        return strtolower($slugger->slug($this->title));
    }

}
