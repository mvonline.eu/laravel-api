<?php

namespace App\Common\Services;

use App\Request\GuzzleRequestAdapter;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Psr\Http\Message\ResponseInterface;

abstract class RestClient
{
    /**
     * @throws GuzzleException
     */
    public function doRequest(string $url, Request $request): mixed
    {
        $response = $this->getResponse($url, $request);

        $stringResponse = (string)$response->getBody();

        return json_decode($stringResponse, true);
    }

    /**
     * @throws GuzzleException
     */
    private function getResponse(string $url, Request $request): ResponseInterface
    {
        $client = GuzzleClient::getClient();

        $requestAdapter = new GuzzleRequestAdapter($request);

        return $client->request($requestAdapter->getMethod(), $url, $requestAdapter->getOptions());
    }
}
