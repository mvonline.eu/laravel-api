<?php

namespace App\Common\Services;

use GuzzleHttp\Client;

class GuzzleClient
{
    private static ?Client $client = null;

    public static function getClient(): Client
    {
        if (self::$client == null){
            self::$client = new Client();
        }

        return self::$client;
    }

    public function __clone()
    {

    }
}
