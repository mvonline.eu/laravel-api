<?php

namespace App\Enums;

enum UploadedFileTypes: string
{
    case GPX = 'gpx';
    case PHOTOGALLERY = 'photo';
}
