<?php

namespace App\Enums;

enum FormMode: string
{
    case MODE_ADD = 'add';

    case MODE_EDIT = 'edit';
}
