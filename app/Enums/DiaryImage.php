<?php

namespace App\Enums;

enum DiaryImage: string
{
    case IMAGE = 'image';
    case LARGE = 'large';
    case MEDIUM = 'medium';
    case THUMB = 'thumb';
}

