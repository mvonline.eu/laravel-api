<?php

namespace App\Enums;

enum GeocacheLogType
{
    const FIND = "Geocache Found";
    const DIDNT_FIND = "Geocache";
    const COORDINATES_OVERRIDE = "Coordinates Override";
}