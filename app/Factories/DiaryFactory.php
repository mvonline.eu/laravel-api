<?php

namespace App\Factories;

use App\Models\Diary;

class DiaryFactory
{
    /**
     * @throws \Exception
     */
    public static function fromFieldValues(array $fieldValues): Diary
    {
        if (isset($fieldValues['id'])){
            $diary = Diary::find($fieldValues['id']);
        } else {
            $diary = new Diary();
        }

        if (isset($fieldValues['title'])) {
            $diary->title = $fieldValues['title'];
        }

        if (isset($fieldValues['keywords'])) {
            $diary->keywords = $fieldValues['keywords'];
        }

        if (isset($fieldValues['from_date'])) {
            $date = new \DateTimeImmutable($fieldValues['from_date']);
            $diary->from_date = $date;
        }

        return $diary;
    }
}

