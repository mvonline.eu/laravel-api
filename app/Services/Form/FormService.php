<?php

namespace App\Services\Form;


use App\Models\Form\FileFormField;
use App\Models\Form\Form;
use App\Models\Form\FormField;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;

class FormService
{

    /**
     * @throws \Exception
     */
    public function loadForm(Request $request): Form
    {
        $form = $request->get('_form');
        $storedForm = Session::get('form-' . $form);

        if ($storedForm instanceof Form) {
            return $storedForm;
        }

        throw new \Exception('Can\'t load form');
    }

    /**
     * @throws \Exception
     */
    public function loadData(Form $form, Request $request): Form
    {
        foreach ($form->getFields() as $field) {
            $value = $this->getRequestValue($request, $field);

            $field->setValue($value);
            $form->setValue($field->getName(), $value);
        }

        return $form;
    }

    /**
     * @throws \Exception
     */
    public function validateForm(Form $form): Form
    {
        //reset errors first
        $this->resetErrors($form);

        foreach ($form->getFields() as $field) {
            $field->validate();
        }

        return $form;
    }

    public function resetErrors(Form $form): Form
    {
        foreach ($form->getFields() as $field) {
            $field->resetErrorMessage();
        }

        return $form;
    }

    /**
     * @throws \Exception
     */
    private function getRequestValue(Request $request, FormField $formField): mixed
    {
        if ($formField->getType() === FormField::TYPE_FILE && $request->hasFile($formField->getName())) {
            /**@var FileFormField $formField */
            return $formField->transformToSerializable($request->file($formField->getName()));
        }

        if ($formField->getType() === FormField::TYPE_DATE) {
            $formValue = $request->get($formField->getName());
            return new \DateTimeImmutable($formValue);
        }

        return $request->get($formField->getName());
    }
}
