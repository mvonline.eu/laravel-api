<?php

namespace App\Services\GeoGetter;

    interface GeoGetter {

        public function getTown();

        public function getRegion();

        public function getDistrict();

        public function getAltitude();

    }

?>
