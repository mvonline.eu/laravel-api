<?php

namespace App\Services\GeoGetter;

use App\Services\GeoGetter\GeoGetter;

class OpenStreetMapsGeoGetter implements GeoGetter
{
    private $lat;

    private $lon;

    private $api;

    public function __construct($lat, $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
        $this->getApi();
    }

    private function getApi()
    {
        $opts = array('http'=>array('header'=>"User-Agent: StevesCleverAddressScript 3.7.6\r\n"));
        $context = stream_context_create($opts);

        $geocode = file_get_contents(sprintf("https://nominatim.openstreetmap.org/reverse?lat=%s&lon=%s&format=json", $this->lat, $this->lon), false, $context);
        $this->api = json_decode($geocode, true);
    }

    public function getRegion()
    {
        $geo = $this->api;
        return $geo['address']['state'] ?? "";
    }

    public function getDistrict()
    {
        $geo = $this->api;
        return $geo['address']['county'] ?? "";
    }

    public function getTown()
    {
        $geo = $this->api;
        return $geo['address']['city_district'] ?? $geo['address']['village'] ?? '';
    }

    public function getAltitude()
    {
        return null;
    }
}
