<?php

namespace App\Services\Geocaching\GPXGTags;

use App\Services\Geocaching\GPXGTags\LocationGPXGParser;

class SlovakGPXGParser extends LocationGPXGParser
{
    public function process(array $xml): void
    {
        $this->district = $xml["SK okres"];
        $this->region = $xml["SK kraj"] ?? "";
        $this->elevation = $xml["Elevation"];
    }
}