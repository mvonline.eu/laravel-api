<?php

namespace App\Services\Geocaching\GPXGTags;

interface GPXGParserInterface
{
    public function process(array $xml);
}