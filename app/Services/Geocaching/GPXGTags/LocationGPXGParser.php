<?php

namespace App\Services\Geocaching\GPXGTags;

use App\Services\Geocaching\GPXGTags\GPXGParserInterface;

class LocationGPXGParser implements GPXGParserInterface
{
    protected $region;

    protected $district;
    protected $elevation;

    protected $town;

    public function process(array $xml): void
    {
        $this->elevation = $xml["Elevation"];
    }



    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return mixed
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * @return mixed
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * @return mixed
     */
    public function getElevation()
    {
        return $this->elevation;
    }
}
