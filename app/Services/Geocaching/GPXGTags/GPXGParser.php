<?php

namespace App\Services\Geocaching\GPXGTags;

use App\Services\Geocaching\GPXGTags\LocationGPXGParser;
use App\Services\Geocaching\GPXGTags\LocationGPXGParser as GPXGTagsLocationGPXGParser;

class GPXGParser
{
    private LocationGPXGParser $locationParser;

    public function __construct(LocationGPXGParser $locationParser)
    {
        $this->locationParser = $locationParser;
    }

    public function process(array $xml)
    {
        $this->locationParser->process($xml);
    }

    public function getRegion()
    {
        return $this->locationParser->getRegion();
    }

    public function getElevation()
    {
        return $this->locationParser->getElevation();
    }

    public function getDistrict()
    {
        return $this->locationParser->getDistrict();
    }

    public function getTown()
    {
        return $this->locationParser->getTown();
    }
}