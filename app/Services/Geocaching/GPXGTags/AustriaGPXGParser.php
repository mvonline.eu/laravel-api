<?php

namespace App\Services\Geocaching\GPXGTags;

use App\Services\Geocaching\GPXGTags\LocationGPXGParser;

class AustriaGPXGParser extends LocationGPXGParser
{
    public function process(array $xml): void
    {
        $this->elevation = $xml["Elevation"];
        $this->district = $xml["AT Bezirk"];
        $this->region = $xml["AT Land"];
    }
}