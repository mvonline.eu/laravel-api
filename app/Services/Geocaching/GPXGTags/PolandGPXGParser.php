<?php

namespace App\Services\Geocaching\GPXGTags;

use App\Services\Geocaching\GPXGTags\LocationGPXGParser;

class PolandGPXGParser extends LocationGPXGParser
{
    public function process(array $xml): void
    {

        // $this->district = $xml["CZ okres"];
        $this->elevation = $xml["Elevation"];
        //$this->region = $xml["CZ kraj"];
        // $this->town = $xml["CZ Mestska cast"] <> "-" ? $xml["CZ Povereny urad"] : $xml["CZ Mestska cast"];
    }
}