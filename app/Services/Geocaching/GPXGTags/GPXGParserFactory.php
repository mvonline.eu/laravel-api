<?php

namespace App\Services\Geocaching\GPXGTags;

use App\Exception\CountryTagsProcessorNotExists;

class GPXGParserFactory
{
    public static function getParserByCountry(string $country)
    {
        switch ($country) {
            case "Slovakia":
                return new SlovakGPXGParser();
                break;
            case "Czechia":
                return new CzechGPXGParser();
                break;
            case "Austria":
                return new AustriaGPXGParser();
                break;
            case "Poland":
                return new PolandGPXGParser();
                break;
            default:
                return new LocationGPXGParser();
        }
    }
}
