<?php

namespace App\Services;

use App\Models\MailingList;
use Illuminate\Support\Str;

class MailingListService {


    public function addEmailToMailingList(string $email)
    {
        $hash = (string)Str::uuid();
        
        $mailing = new MailingList([
            'email' => $email,
            'removed' => 0,
            'hash' => $hash,
        ]);

        $mailing->save();

        return $mailing;
    }

    public function getAllMailingLists()
    {
        $mailingList = MailingList::where('removed', 0)->get();

        return $mailingList;
    }

}