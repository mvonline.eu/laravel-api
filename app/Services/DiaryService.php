<?php

namespace App\Services;

use App\Events\DiaryCreatedEvent;
use App\Http\Requests\DiaryRequest;
use App\Methods\DiaryMethods;
use App\Models\Diary;
use App\Models\TripType;
use Illuminate\Support\Facades\Request;

class DiaryService {

    public function createDiary(DiaryRequest $request)
    {
        $diary = new Diary($request->validated());

        $diary->from_date = new \DateTime($request->get('date'));

        $diary->save();

        $request->uploadFiles($diary);

        $tripTypes = $this->parseTripTypes($request->get('type') ?? []);

        $diary->tripTypes()->detach();
        foreach ($tripTypes as $type){
            $diary->tripTypes()->attach($type);
        }

        $diary->load('tripTypes');
        $diary->load('uploadedFiles');

        DiaryCreatedEvent::dispatch($diary);

        return $diary;
    }


    public function parseTripTypes(?array $tripTypes = []): array
    {

        $toRet = [];

        foreach ($tripTypes as $type) {
            $tripType = TripType::where('id', $type)->first();

            
            if ($tripType){
                $toRet[] = $tripType;
            }
        }
        
        return $toRet;
    }

}