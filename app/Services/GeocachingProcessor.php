<?php

namespace App\Services;

use App\Enums\GeocacheLogType as EnumsGeocacheLogType;
use App\Models\Geocaching;
use App\Models\GeocachingCountry;
use App\Models\GeocachingType;
use App\Services\Geocaching\GPXGTags\GPXGParser as GPXGTagsGPXGParser;
use App\Services\Geocaching\GPXGTags\GPXGParserFactory as GPXGTagsGPXGParserFactory;
use App\Services\GeoGetter\OpenStreetMapsGeoGetter;
use Illuminate\Support\Facades\Log;

class GeocachingProcessor
{

    public function processWaypoint($waypoint, int $lastNumber)
    {
        $geocache = null;
        try {
            $sym = (string)$waypoint->wpt->sym;
            $id = (string)$waypoint->wpt->name;

            $existsGeocache = Geocaching::find($id);

            if ($existsGeocache){
                return;
            }

            Log::info("Processing Geocache #{$id}");

            if ($sym == EnumsGeocacheLogType::DIDNT_FIND) {
                Log::info("Skipping Geocache because it is not found: #{$id}");
                return;
            }

            $lat = (string)$waypoint->wpt->{'@attributes'}->lat;
            $lon = (string)$waypoint->wpt->{'@attributes'}->lon;

            if ($lat == "0" && $lon == "0") {
                return;
            }
            $desc = (string)$waypoint->wpt->desc;

            if ($desc === EnumsGeocacheLogType::COORDINATES_OVERRIDE) {
                return;
            }

            // $extensions = $waypoint->wpt->extensions;
            $groundspeak = $waypoint->groundspeak?->cache;

            $name = (string)$groundspeak->name;
            $terrain = (string)$groundspeak->terrain;
            $difficulty = (string)$groundspeak->difficulty;
            $owner = (string)$groundspeak->owner;
            $type = (string)$groundspeak->type;
            $country = (string)$groundspeak->country;
            $state = (string)$groundspeak->state;

            $geogetExtension = $waypoint->gpxg?->GeogetExtension;
            $found = new \DateTime((string)$geogetExtension->Found);
            $tags = json_decode(json_encode($waypoint->tags), true);


            $typeEntity = GeocachingType::where('id_name', $type)->firstOrFail();
            $countryEntity = GeocachingCountry::where('original_name', $country)->firstOrFail();

           
            $geocache = new Geocaching([
                'id' => $id,
                'number' => $lastNumber +1,
                'lat' => (string)$lat,
                'lon' => (string)$lon,
                'name' => $name,
                'type' => $typeEntity->id,
                'difficulty' => $difficulty,
                'terrain' => $terrain,
                'owner' => $owner,
                'found' => $found,
                'country' => $countryEntity->id,
                'region' => $state
            ]);

            $locationParser = GPXGTagsGPXGParserFactory::getParserByCountry($country);
            if ($locationParser) {
                $gpxgParser = new GPXGTagsGPXGParser($locationParser);
                $gpxgParser->process($tags);

                $geocache->elevation = $gpxgParser->getElevation();
                if (!$geocache->region) {
                    $geocache->region = $gpxgParser->getRegion();
                }
                $geocache->district = $gpxgParser->getDistrict();
            }

            if (!$geocache->district || !$geocache->region) {
                $geoGetter = new OpenStreetMapsGeoGetter($lat, $lon);

                $region = $geoGetter->getRegion();
                $district = $geoGetter->getDistrict();

                if (!$geocache->district) {
                    $geocache->district = $district;
                }
                if (!$geocache->region) {
                    $geocache->region = $region;
                }
            }

            Log::info("Finished processing Geocache #{$id}");
            return $geocache;
        } catch (\Exception $ex) {
            dump($ex, $geocache, $ex->getMessage());
            Log::error($geocache?->id . ":" . $ex->getMessage());
        }
    }
}
