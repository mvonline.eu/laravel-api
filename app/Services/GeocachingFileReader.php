<?php 

namespace App\Services;

class GeocachingFileReader
{

    private $file;
    private $content;

    public function setFile($file)
    {
        $this->file = $file;
        
    }

    public function setContent($content)
    {
        $this->content = $content;
        
    }

    public function run()
    {
        $this->processFile();
    }

    private function processFile()
    {
        if ($this->checkContent()){
            $this->content = preg_replace('#&#', '&amp;', $this->content);
            $this->removeCdata();
            $this->content = simplexml_load_string($this->content);

            $waypoints = $this->content->wpt;


            $i=0;
            foreach ($waypoints as $wpt)
		    {
                // Register the 'gpxg' namespace
                $wpt->registerXPathNamespace('gpxg', 'https://www.geoget.cz/GpxExtensions/v2');

                $tags = $wpt->xpath('.//gpxg:Tag');
                try {
                    $toRet[$i]['wpt'] = $wpt;
                    $namespaces = $wpt->getNamespaces(true);
                    $toRet[$i]['groundspeak'] = $wpt->extensions->children($namespaces['groundspeak']);
                    $toRet[$i]['gpxg'] = $wpt->extensions->children($namespaces['gpxg']);
                    $toRet[$i]['tags'] = $this->extractTags($tags);
                    $i++;
                } catch (\Exception $e){
                    continue;
                }

            }


		    $this->content = $toRet;
        }
    }

    private function extractTags($tags) {
        $ret = [];
        foreach ($tags as $tag){
            $category = (string) $tag->attributes()->Category;
            $value = (string) $tag;
            $ret[$category] = $value;
        }

        return $ret;
    }

    public function checkContent(){
        $test = preg_match("/Geoget data file/", $this->content);

        if (!$test){
            throw new \Exception("Inserted XML file did not come from Geoget");
        }

        return TRUE;
    }

    private function removeCdata() {

		$this->content = str_replace('<![CDATA[', '', $this->content);
		$this->content = str_replace(']]>', '', $this->content);
	}


    public function getGeocaches(){
        return $this->content;
    }

}

