<?php 

namespace App\Services;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQService {

    private static $connection = null;
    private static $channel = null;

    public static function getConnection()
    {
        if (!self::$connection){
            self::$connection = new AMQPStreamConnection(env("RABBIT_CONNECTION"), env("RABBIT_PORT"), env("RABBIT_USER"), env("RABBIT_PASSWORD"));
        }
        return self::$connection;
    }

    public static function getChannel()
    {
        $connection = self::getConnection();
        return $connection->channel();
    }

    public static function publishMessage($amqpMessage, string $queue)
    {
        try {
            $channel = self::getChannel();
            if (!$amqpMessage instanceof AMQPMessage){
                $amqpMessage = new AMQPMessage($amqpMessage, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
            } 

            $channel->basic_publish($amqpMessage, '', $queue);
        } catch (\Exception $ex){
            throw new \Exception($ex);
        } finally {
            self::closeChannel();
            self::closeConnection();
        }
    }

    public static function publishMessages(array $amqpMessages, string $queue, $json = false)
    {
        try {
            self::createQueue($queue);
            $channel = self::getChannel();
            foreach ($amqpMessages as $amqp){
                if (!$amqp instanceof AMQPMessage){
                    $amqpMessage = new AMQPMessage($json ? json_encode($amqp) : $amqp, ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);
                }

                $channel->basic_publish($amqpMessage, '', $queue);
            }
        } catch (\Exception $ex){
            throw new \Exception($ex);
        } finally {
            self::closeChannel();
            self::closeConnection();   
        }
    }

    public static function createQueue($queueName)
    {
        self::getChannel()->queue_declare($queueName, false, false, false, false);
    }

    public static function closeChannel()
    {
        self::$channel?->close();
    }

    public static function closeConnection()
    {
        self::$connection->close();
    }

    public static function close()
    {
        self::closeChannel();        
        self::closeConnection();    
        self::$connection = null;    
    }
    

    

}