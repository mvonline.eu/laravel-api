<?php

namespace App\Services;

use App\Models\API\Response\DiaryGeoDataResponse;
use App\Models\API\Response\GPXPolyline;
use App\Models\API\Response\NominatimLocationDataResponse;
use App\Rest\Nominatim\NominatimFactory;
use App\Rest\Nominatim\Reverse;
use App\Rest\Services\APIService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Arr;
use phpGPX\Models\GpxFile;
use phpGPX\phpGPX;

class GPXService
{

    private GpxFile $gpxFile;

    private phpGPX $gpx;

    public function __construct()
    {
        $this->gpx = new phpGPX();
    }

    private function isFileOpenable($file): bool
    {
        return @file_get_contents($file) !== false;
    }


    /**
     * @throws FileNotFoundException
     */
    public function loadGpx($file): void
    {
        //try to open file
        if (!$this->isFileOpenable($file)) {
            throw new FileNotFoundException("GPX File $file is not found or not readable");
        }
        $this->gpxFile = $this->gpx->load($file);
    }

    /**
     * @throws GuzzleException
     */
    public function getLocationData(float $lat, float $lng): NominatimLocationDataResponse
    {
        $nominatim = NominatimFactory::fromReverse($lat, $lng, Reverse::ZOOM_BUILDING);

        /** @var APIService $apiService */
        $apiService = resolve(APIService::class);
        $locations = $apiService->doRequest($nominatim);
        return NominatimLocationDataResponse::createFromLocation($locations['address']);
    }

    /** @return GPXPolyline[] */
    public function getPolylines(): array
    {
        $tracks = $this->gpxFile->tracks[0]->segments[0]->getPoints();

        $tracksArr = [];
        foreach ($tracks as $track) {
            $tracksArr[] = new GPXPolyline(
                $track->longitude,
                $track->latitude,
                $track->elevation,
                $track->distance,
            );
        }

        return $tracksArr;
    }

    /**
     * @throws GuzzleException
     */
    public function getStats(): DiaryGeoDataResponse
    {
        $track = Arr::first($this->gpxFile->tracks);
        $stats = $track->stats->toArray();

        $startedCoords = $stats['startedAtCoords'];
        $finishCoords = $stats['finishedAtCoords'];

        $locationsData = DiaryGeoDataResponse::fromStats($stats);

        $start = $this->getLocationData($startedCoords['lat'], $startedCoords['lng']);
        $end = $this->getLocationData($finishCoords['lat'], $finishCoords['lng']);

        $locationsData->setStart($start);
        $locationsData->setEnd($end);

        return $locationsData;
    }


}
