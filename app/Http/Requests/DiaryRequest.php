<?php

namespace App\Http\Requests;

use App\Enums\DiaryImage;
use App\Enums\UploadedFileTypes;
use App\Methods\DiaryFileUploader;
use App\Methods\DiaryFileUploader\GPXFileUploader;
use App\Methods\DiaryFileUploader\PhotogalleryFileUploader;
use App\Models\Diary;
use App\Models\UploadedFile;
use Illuminate\Foundation\Http\FormRequest;

class DiaryRequest extends FormRequest
{
    private GPXFileUploader $gpxFileUploader;
    private PhotogalleryFileUploader $photogalleryFileUploader;

    public function __construct(GPXFileUploader $gpxFileUploader, PhotogalleryFileUploader $photogalleryFileUploader)
    {
        $this->gpxFileUploader = $gpxFileUploader;
        $this->photogalleryFileUploader = $photogalleryFileUploader;
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {

        $rules = [
            'keywords' => 'string',
            'gpx' => 'file|mimes:xml,gpx+xml,text/xml',
            'photosFile' => 'file|mimes:zip',
            'type' => 'array',
            'type.*' => 'integer'
        ];

        if (request()->isMethod('PUT')){
           $rules['date'] = 'date_format:Y-m-d H:i:s\\Z';
           $rules['title'] = 'string';
        } else {
            $rules['date'] = 'required|date_format:Y-m-d H:i:s\\Z';
            $rules['title'] = 'required|string';
        }

        return $rules;
    }

    public function uploadFiles(Diary $diary)
    {
        $gpx = $this->file('gpx');
        $photos = $this->file('photosFile');

        if ($gpx){
            $this->gpxFileUploader->upload($gpx, $diary);
            $file = $this->gpxFileUploader->makeEntity($diary);

            $diary->gpx()->delete();

            $diary->gpx()->attach($file);

        }

        if ($photos){
            $this->photogalleryFileUploader->upload($photos, $diary);
            $entities = $this->photogalleryFileUploader->makeEntity($diary);

            $diary->photogallery()->delete();

            foreach ($entities as $entity){
                $diary->photogallery()->attach($entity);
            }
        }

        $diary->save();
    }

}
