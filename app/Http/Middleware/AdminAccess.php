<?php

namespace App\Http\Middleware;

use App\Enums\UserRoles;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $hasAccess = $request->user()->hasRole(UserRoles::ADMIN->value);
        if ($hasAccess) {
            return $next($request);
        }
        return response()->json(['error' => 'Unauthorized'], 403);
    }
}
