<?php

namespace App\Http\Controllers;

use App\Models\MailingList;
use App\Services\MailingListService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MailingController extends Controller
{

    private MailingListService $mailingListService;

    public function __construct(MailingListService $mailingListService)
    {
        $this->mailingListService = $mailingListService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }


    public function add(Request $request)
    {
        $request->validate([
            'email:required|email|unique'
        ]);

        $email = $request->get('email');

        $mailUser = MailingList::where(['email' => $request->get('email'), 'removed' => 0])->first();

        if ($mailUser){
            return response()->json(['message' => 'User with email already exists'], 409);
        }

        $mailing = $this->mailingListService->addEmailToMailingList($email);

        return response()->json($mailing, 200);

    }


    public function delete(Request $request, string $hash)
    {
        $mailing = MailingList::where(['hash' => $hash, 'removed' => 0])->first();

        if (!$mailing){
            return redirect('/');
        }
        
        $mailing->update([
            'hash' => null,
            'removed' => 1
        ]);

        return redirect('/mailing/removed');
    }
}
