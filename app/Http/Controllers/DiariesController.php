<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiariesListRequest;
use App\Http\Requests\DiariesSearchRequest;
use App\Models\API\Response\DiaryDetailResponse;
use App\Models\Diary;
use App\Services\GPXService;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use OpenApi\Annotations as OA;


class DiariesController extends Controller
{

    /**
     * @OA\Get(
     *     path="/api/diaries/list",
     *     summary="List Diaries",
     *     description="Returns a list of diaries with optional filters and sorting.",
     *     operationId="getDiariesList",
     *     tags={"Diaries"},
     *     @OA\Parameter(
     *          name="query",
     *          in="query",
     *          required=false,
     *          description="Query parameters as an object.",
     *          @OA\Schema(ref="#/components/schemas/DiariesListQuery")
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="List of diaries",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 @OA\Property(property="id", type="integer", example=1),
     *                 @OA\Property(property="title", type="string", example="Rysy"),
     *                 @OA\Property(property="from_date", type="string", format="date", example="2025-01-01"),
     *                 @OA\Property(property="image", type="string", nullable=true,
     *     example="https://example.com/image.jpg"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Validation error or other exception
     *     message.")
     *         )
     *     )
     * )
     */
    public function list(DiariesListRequest $request): JsonResponse
    {
        try {
            $validatedData = $request->validated();

            $diaries = Diary::query();

            if ($request->has('offset')) {
                $diaries->skip($request->get('offset'));
            }

            if ($request->has('limit')) {
                $diaries->take($request->get('limit'));
            }

            if ($request->has('orderBy') || $request->has('direction')) {
                if ($request->get('orderBy') === 'date') {
                    $diaries->orderBy('from_date', $request->get('direction', 'asc'));
                } else {
                    $diaries->orderBy($request->get('orderBy', 'from_date'), $request->get('direction', 'asc'));
                }
            }

            if ($request->has('withPhotos') && $request->get('withPhotos', false)) {
                $diaries->with([
                    'uploadedFiles' => function ($query) {
                        return $query->where('type', '=', 'photo');
                    },
                ]);
            }

            $diaries = $diaries->get();

            return response()->json($diaries);
        } catch (Exception $e) {
            return response()->json($e->getMessage(), 400);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/diaries/search",
     *     summary="Search diaries",
     *     description="Returns a list of diaries",
     *     operationId="getDiariesSearch",
     *     tags={"Diaries"},
     *     @OA\Parameter(
     *          name="query",
     *          in="query",
     *          required=true,
     *          description="Search",
     *          @OA\Schema(type="string"),
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="List of diaries",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(
     *                 type="object",
     *                 @OA\Property(property="id", type="integer", example=1),
     *                 @OA\Property(property="title", type="string", example="Rysy"),
     *                 @OA\Property(property="from_date", type="string", format="date", example="2025-01-01"),
     *                 @OA\Property(property="image", type="string", nullable=true,
     *     example="https://example.com/image.jpg"),
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad request",
     *         @OA\JsonContent(
     *             @OA\Property(property="error", type="string", example="Validation error or other exception
     *     message.")
     *         )
     *     )
     * )
     */
    public function search(DiariesSearchRequest $request): JsonResponse
    {
        $diaries = Diary::query();
        $diaries = $diaries
            ->where('keywords', 'like', '%' . $request->query('query') . '%')
            ->orderBy('from_date', 'desc');

        return response()->json($diaries->get());
    }

    /**
     * @OA\Get(
     *     path="/api/diaries/{id}",
     *     summary="Get diary details",
     *     description="Retrieves details of a specific diary entry by its ID.",
     *     operationId="getDiaryDetail",
     *     tags={"Diaries"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the diary entry",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful response",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="id", type="integer", example=1),
     *             @OA\Property(property="title", type="string", example="Trip to the mountains"),
     *             @OA\Property(property="date", type="string", format="date", example="2024-02-01"),
     *             @OA\Property(property="slug", type="string", example="265-rysy"),
     *             @OA\Property(property="tripTypes", type="array",
     *                 @OA\Items(ref="#/components/schemas/DiaryTripTypesResponse")
     *             ),
     *             @OA\Property(property="photogallery", type="array",
     *                 @OA\Items(type="string", example="https://example.com/photo1.jpg")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Diary entry not found"
     *     )
     * )
     * @OA\Schema(
     *     schema="DiaryTripTypesResponse",
     *     @OA\Property(property="name", type="string", example="Hiking"),
     *     @OA\Property(property="icon", type="string", example="fas fa-hiking"),
     * )
     * @throws Exception
     */
    public function detail(int $id): JsonResponse
    {
        /** @var Diary $diary */
        $diary = Diary::with(['uploadedFiles', 'tripTypes'])->findOrFail($id);

        return response()->json(DiaryDetailResponse::fromDiary($diary));
    }

    /**
     * @OA\Get(
     *     path="/api/diaries/{id}/geodata",
     *     summary="Get diary geodata",
     *     description="Retrieves geodata of a specific diary entry by its ID.",
     *     operationId="getDiaryGeoData",
     *     tags={"Diaries"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the diary entry",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful response",
     *          @OA\JsonContent(ref="#/components/schemas/DiaryGeoDataResponse")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Diary entry not found"
     *     )
     * )
     */
    public function geoData(int $id): JsonResponse
    {
        $diary = Diary::with(['uploadedFiles'])->findOrFail($id);

        if (!$diary) {
            return response()->json('Diary not Found', 404);
        }

        /** @var Diary $diary */
        $gpxFile = $diary->uploadedFiles()->where('type', '=', 'gpx')->take(1)->first();
        try {
            $gpx = new GPXService();
            $gpx->loadGpx(storage_path($gpxFile->path));
            $polylines = $gpx->getPolylines();
            $result = $gpx->getStats();
            $result->setPolylines($polylines);
            return response()->json($result->jsonSerialize());
        } catch (FileNotFoundException $e) {
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        } catch (GuzzleException $e) {
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 500);
        }
    }
}
