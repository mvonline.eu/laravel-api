<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * @OA\OpenApi(
 * @OA\Info(
 * title="Your API Title",
 * version="1.0.0",
 * description="This is the API documentation for your project.",
 * @OA\Contact(
 * email="developer@example.com"
 * ),
 * @OA\License(
 * name="MIT",
 * url="https://opensource.org/licenses/MIT"
 * )
 * ),
 * @OA\Server(
 * url="http://mvonline.local",
 * description="Development Server"
 * )
 * )
 */
class Controller extends BaseController
{

    use AuthorizesRequests;
    use ValidatesRequests;
}
