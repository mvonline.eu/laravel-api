<?php

namespace App\Http\Controllers;

use App\Methods\DiaryFileUploader\GPXFileUploader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GeocachingController extends Controller
{

    private GPXFileUploader $gpxFileUploader;
   
    public function __construct(GPXFileUploader $gpxFileUploader)
    {
        $this->gpxFileUploader = $gpxFileUploader;
    }

    public function import(Request $request)
    {
        $file = $request->file('file');
        
        $uploadPath = getenv("GEOGET_PATH");

        try {
            $this->gpxFileUploader->uploadFile($file, $uploadPath, 'geoget.gpx');
            return response()->json(['message' => 'The file has been uploaded']);
        } catch (\Exception $ex){
            Log::error($ex->getMessage());
            return response()->json(['error' => 'Upload file failed'], 500);
        }
    }

}
