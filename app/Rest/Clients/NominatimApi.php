<?php

namespace App\Rest\Clients;

use App\Rest\Nominatim\NominatimQuery;
use Symfony\Component\HttpFoundation\Request;

class NominatimApi implements RestRequestInterface
{

    private NominatimQuery $query;

    public function __construct(NominatimQuery $query) {
        $this->query = $query;
    }

    public function getHost(): string
    {
        return 'https://nominatim.openstreetmap.org';
    }

    public function getPath(): string
    {
        return $this->query->getPath();
    }

    public function getQuery(): array
    {
        return $this->query->getQuery();
    }

    public function getMethod(): string
    {
        return Request::METHOD_GET;
    }

    public function getHeaders(): array
    {
        return [
            'User-Agent' => 'MvOnline.eu (martin.vrabec@hotmail.com)',
        ];
    }
}
