<?php

namespace App\Rest\Clients;

interface RestRequestInterface
{
    public function getHost(): string;

    public function getPath(): string;

    public function getQuery(): array;

    public function getMethod(): string;

    public function getHeaders(): array;
}
