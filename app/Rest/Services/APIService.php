<?php

namespace App\Rest\Services;

use App\Rest\Clients\RestRequestInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class APIService
{
    /**
     * @throws GuzzleException
     */
    public function doRequest(RestRequestInterface $rest): array
    {
        $response = $this->getResponse($rest);

        $stringResponse = $response->getBody();

        return json_decode($stringResponse, true);
    }

    /**
     * @throws GuzzleException
     */
    private function getResponse(RestRequestInterface $rest): ResponseInterface
    {
        $client = new Client([
            'base_uri' => $rest->getHost(),
        ]);
        return $client->request($rest->getMethod(), $rest->getPath(), [
            'query' => $rest->getQuery(),
            'headers' => $rest->getHeaders()
        ]);
    }
}
