<?php

namespace App\Rest\Nominatim;

class Reverse extends NominatimQuery
{
    public const ZOOM_CONTINENT = 0;
    public const ZOOM_COUNTRY = 3;
    public const ZOOM_STATE = 5;
    public const ZOOM_REGION = 6;
    public const ZOOM_COUNTY = 8;
    public const ZOOM_CITY = 10;
    public const ZOOM_TOWN = 12;
    public const ZOOM_VILLAGE = 13;
    public const ZOOM_NEIGHBOURHOOD = 14;
    public const ZOOM_LOCALITY = 15;
    public const ZOOM_MAJOR_STREET = 16;
    public const ZOOM_MINOR_STREET = 17;
    public const ZOOM_BUILDING = 18;

    public function setZoom(int $zoom): self
    {
        $this->query['zoom'] = $zoom;
        return $this;
    }

    public function setLatLng(string $lat, string $lon): self
    {
        $this->query['lat'] = $lat;
        $this->query['lon'] = $lon;
        return $this;
    }

}
