<?php

namespace App\Rest\Nominatim;

use App\Rest\Clients\NominatimApi;

class NominatimFactory
{
    public static function fromReverse(string $lat, string $lng, int $zoom): NominatimApi
    {
        $reverse = new Reverse();
        $reverse
            ->setPath('reverse')
            ->setLatLng($lat, $lng)
            ->setZoom($zoom);

        return new NominatimApi($reverse);
    }
}
