<?php

namespace App\Rest\Nominatim;

abstract class NominatimQuery
{
    protected string $path;

    protected ?array $query = [];

    protected string $format;

    public function __construct(array $query = [])
    {
        if (empty($query['format'])) {
            $query['format'] = 'json';
        }

        $this->setQuery($query);
        $this->setFormat($query['format']);
    }

    /**
     * @return mixed
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function getQuery(): array
    {
        return $this->query;
    }

    public function getQueryString(): string
    {
        return http_build_query($this->query);
    }

    public function setQuery(array $query): self
    {
        $this->query = $query;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param mixed $format
     */
    public function setFormat(string $format): self
    {
        $this->format = $format;
        return $this;
    }
}
