<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TripTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tripTypes = config('tripTypes');

        foreach ($tripTypes as $tripType) {
            DB::table('trip_types')->insert([
                'text' => $tripType['name'],
                'icon' => $tripType['icon'],
            ]);
        }
    }
}
