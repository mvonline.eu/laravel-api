<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\GeocachingCountry;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            TripTypeSeeder::class,
            GeocachingTypeSeeder::class,
            GeocachingCountrySeeder::class,
            AdminSeeder::class,
        ]);
    }
}
