<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeocachingTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('geocaching_type')->insert([[
            'id' => 1,
            'id_name' => 'Earthcache',
            'name' => 'Earth Cache',
            'position' => 1,
            'image' => 'earth.svg',
            'color' => '#61d2ec'
        ], [
            'id' => 2,
            'id_name' => 'Traditional Cache',
            'name' => 'Tradičná Cache',
            'position' => 2,
            'image' => 'traditional.svg',
            'color' => '#4e7932'
        ], [
            'id' => 3,
            'id_name' => 'Multi-cache',
            'name' => 'Multi-Cache',
            'position' => 3,
            'image' => 'multi.svg',
            'color' => '#fdd617'
        ], [
            'id' => 4,
            'id_name' => 'Unknown Cache',
            'name' => 'Mystery Cache',
            'position' => 4,
            'image' => 'mystery.svg',
            'color' => '#1d3079'
        ], [
            'id' => 5,
            'id_name' => 'Webcam Cache',
            'name' => 'Webcam Cache',
            'position' => 5,
            'image' => 'webcam.svg',
            'color' => '#c4c4c4'
        ], [
            'id' => 6,
            'id_name' => 'Letterbox Hybrid',
            'name' => 'Letter Box',
            'position' => 6,
            'image' => 'letterbox.svg',
            'color' => '#bfbfff'
        ], [
            'id' => 7,
            'id_name' => 'Virtual Cache',
            'name' => 'Virtual Cache',
            'position' => 7,
            'image' => 'virtual.svg',
            'color' => '#fff'
        ], [
            'id' => 8,
            'id_name' => 'Wherigo Cache',
            'name' => 'Wherigo',
            'position' => 8,
            'image' => 'wherigo.svg',
            'color' => ''
        ]]);
    }
}