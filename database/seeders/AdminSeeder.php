<?php

namespace Database\Seeders;

use App\Enums\UserRoles;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $password = getenv("APP_ENV") == "local" ? "password" : "S4bHUMSjgntOAsW";

        User::create([
            'name' => 'Admin',
            'email' => 'admin@local',
            'password' => Hash::make($password),
            'remember_token' => false,
            'roles' => [UserRoles::ADMIN]
        ]);
    }
}
