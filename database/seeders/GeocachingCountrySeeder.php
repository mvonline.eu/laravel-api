<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use function PHPSTORM_META\map;

class GeocachingCountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('geocaching_country')->insert([[
            'id' => 1,
            'country' => 'Poľsko',
            'image' => 'Poland.png',
            'original_name' => 'Poland'
        ], [
            'id' => 2,
            'country' => 'Slovensko',
            'image' => 'Slovakia.png',
            'original_name' => 'Slovakia'
        ], [
            'id' => 3,
            'country' => 'Rakúsko',
            'image' => 'Austria.png',
            'original_name' => 'Austria'
        ], [
            'id' => 4,
            'country' => 'Česká republika',
            'image' => 'Czech-Republic.png',
            'original_name' => 'Czechia'
        ], [
            'id' => 5,
            'country' => 'Nemecko',
            'image' => 'Germany.png',
            'original_name' => 'Germany'
        ]]);
    }
}
