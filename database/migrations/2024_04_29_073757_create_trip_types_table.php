<?php

use App\Models\Diary;
use App\Models\TripType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('trip_types', function (Blueprint $table) {
            $table->id();
            $table->string('text');
            $table->string('icon');
        });

        Schema::create('diary_trip_type', function(Blueprint $table) {
            $table->foreignIdFor(Diary::class, 'diary_id');
            $table->foreignIdFor(TripType::class, 'trip_type_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('trip_types');
    }
};
