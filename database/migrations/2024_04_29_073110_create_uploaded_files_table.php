<?php

use App\Models\Diary;
use App\Models\UploadedFile;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('uploaded_file', function (Blueprint $table) {
            $table->id();
            $table->string('path');
            $table->string('type');
            $table->json('info')->nullable();
            $table->timestamps();
        });

        Schema::create('diary_uploaded_file', function(Blueprint $table) {
            $table->foreignIdFor(Diary::class, 'diary_id');
            $table->foreignIdFor(UploadedFile::class, 'uploaded_file_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('uploaded_file');
        Schema::dropIfExists('diary_uploaded_file');
    }
};
