<?php

use App\Models\GeocachingCountry;
use App\Models\GeocachingType;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('geocaching', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->string('number');
            $table->string('name');
            $table->foreignIdFor(GeocachingType::class,'type');
            $table->foreignIdFor(GeocachingCountry::class,'country');
            $table->string('owner');
            $table->double('lon');
            $table->double('lat');
            $table->string('region')->nullable();
            $table->string('district')->nullable();
            $table->string('town')->nullable();
            $table->integer('altitude')->nullable();
            $table->dateTime('found');
            $table->double('difficulty');
            $table->double('terrain');
            $table->integer('elevation');
        });

        Schema::create('geocaching_type', function (Blueprint $table) {
            $table->id();
            $table->string('id_name');
            $table->string('name');
            $table->integer('position');
            $table->string('image');
            $table->string('color');
        });

        Schema::create('geocaching_country', function (Blueprint $table) {
            $table->id();
            $table->string('country');
            $table->string('image');
            $table->string('original_name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mailing_lists');
        Schema::dropIfExists('geocaching_type');
        Schema::dropIfExists('geocaching_country');
    }
};
