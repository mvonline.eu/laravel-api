/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade',
    ],
    theme: {
        extend: {
            boxShadow: {
                'mv': '0px 0px 4px 0px rgb(0 0 0 / 75%)',
            },
            colors: {
                'mvonline': '#435d43',
                'mvonline-lighter': '#546c50',
                'mvonline-checked': '#CBF4CB',
            },
        },
    },
    plugins: [],
}
