<?php

namespace Tests;

use App\Models\User;

class TestHelper
{

    public static function createToken(User $user): string
    {
        $token = $user->createToken($user->email.'-AuthToken')->plainTextToken;

        return $token;
    }

    public static function getAdmin(): User
    {
        $user = User::firstOrCreate([
            'email' => 'root@localhost',
            'password' => 'password',
            'roles' => ['ADMIN']
        ]);

        return $user;
    }
    
}
