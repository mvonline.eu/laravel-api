<?php

namespace Tests\Feature;

use App\Services\GeocachingFileReader;
use App\Services\GeocachingProcessor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\File;
use Tests\TestCase;

class GeocachingImportTest extends TestCase
{
    use RefreshDatabase;

    private GeocachingFileReader $gcfr;

    protected $waypoints = [];

    public function setUp(): void
    {
        parent::setUp();
        $this->artisan('db:seed');
        $this->gcfr = new GeocachingFileReader();
    }

    /**
     * A basic feature test example.
     */
    public function testFileRead(): void
    {
        $wpts = $this->loadCachesFromFile();

        $this->assertCount(1186, $wpts);
        $this->assertEquals($wpts[3]->wpt->name, "GC1BRF5");
        $this->assertObjectHasProperty("wpt", $wpts[3]);
        $this->assertObjectHasProperty("groundspeak", $wpts[3]);
        $this->assertObjectHasProperty("gpxg", $wpts[3]);
        $this->assertObjectHasProperty("tags", $wpts[3]);
    }

    public function  testGeocachingProcessor()
    {
        $wpts = $this->loadCachesFromFile();

        $geocachingProcessor = new GeocachingProcessor();
        $geocache = $geocachingProcessor->processWaypoint($wpts[3], 22);


        $this->assertEquals($geocache->number, 23);
        $this->assertEquals($geocache->name, "Demanovska Ice Cave");
    }

    private function loadCachesFromFile()
    {
        $content = file_get_contents("tests/geoget/GEOGET.gpx");

        $this->gcfr->setContent($content);
        $this->gcfr->run();

        $wpts = $this->gcfr->getGeocaches();

        $wpts = json_decode(json_encode($wpts),false);

        return $wpts;
    }
}
