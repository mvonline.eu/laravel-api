<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    // public function testUserCantLogin(): void
    // {
    //     $user = new User(
    //         [
    //             'email' => 'test@localhost',
    //             'password' => Hash::make('password')
    //         ]
    //     );

    //     $response = $this->post('/api/admin/login',[
    //         'email' => 'hello@localhost',
    //         'password' => 'password'
    //     ]);

    //     $response->assertStatus(401);

    //     $response2 = $this->post('/api/admin/login',[
    //         'email' => 'hello@localhost',
    //         'password' => 'password2'
    //     ]);

    //     $response2->assertStatus(401);

    //     $response3 = $this->post('/api/admin/login',[
    //         'email' => 'test@localhost',
    //         'password' => 'password'
    //     ]);

    //     $response3->assertStatus(401);
    // }

    public function testUserCanLogin()
    {
        $user = User::create([
            'email' => 'test@localhost',
            'password' => Hash::make('password'),
            'roles' => ['ADMIN']
        ]);

        $response = $this->post('/api/admin/login', [
            'email' => 'test@localhost',
            'password' => 'password'
        ]);

        $response->assertStatus(200);

        $this->assertArrayHasKey('access_token', $response->original);
    }
}
