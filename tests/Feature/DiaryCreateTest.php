<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Tests\TestHelper;

class DiaryCreateTest extends TestCase
{

    use RefreshDatabase;

    private $url = '/api/admin/diaries';

    private $token;

    private $user;

    private $json = [
        'date' => '2022-04-14 17:11:22',
        'title' => 'SAGFDF',
        'type' => [1, 2],
        'keywords' => 'keywords'
    ];

    public function setUp(): void
    {
        parent::setUp();

        $user = TestHelper::getAdmin();

        $this->token = TestHelper::createToken($user);
    }

    /**
     * A basic feature test example.
     */
    public function testDate(): void
    {
        $response = $this->getResponse($this->url, $this->json);
        $this->assertValidationError($response, 'date');
    }

    public function testDate2()
    {
        $this->json['date'] = '14.9.2024';

        $response = $this->getResponse($this->url, $this->json);
        $this->assertValidationError($response, 'date');
    }

    public function testBadLeapDate()
    {
        $this->json['date'] = '2021-02-29 11:00:00Z';

        $response = $this->getResponse($this->url, $this->json);

        $this->assertValidationError($response, 'date');
    }

    public function testGoodDate4()
    {
        $this->json['date'] = '2024-02-29 17:11:22Z';

        $response = $this->getResponse($this->url, $this->json);
        $response->assertStatus(201);

        $this->json['date'] = '2024-02-17 17:11:22Z';
    }

    public function testOverflowDate()
    {
        $this->json['date'] = '2021-12-32 11:00:00Z';

        $response = $this->getResponse($this->url, $this->json);

        $this->assertValidationError($response, 'date');
    }

    public function testEmptyTitle(): void
    {
        $this->json['title'] = '';

        $response = $this->getResponse('/api/admin/diaries', $this->json);

        $this->assertValidationError($response, 'title');
    }

    public function testUnsetTitle(): void
    {
        $this->resetDate();
        $json = $this->json;
        unset($json['title']);

        $this->assertArrayNotHasKey('title', $json);

        $response = $this->getResponse('/api/admin/diaries', $json);

        $this->assertValidationError($response, 'title');
    }

    private function resetDate()
    {
        $this->json['date'] = '2022-04-14 17:11:22Z';
    }

    private function getResponse($url, $data)
    {
        return $this->withToken($this->token)->postJson($url, $data);
    }

    private function assertValidationError($response, $field)
    {
        $response->assertStatus(422);
        $data = $response->getData();
        $this->assertTrue(isset($data?->errors?->{$field}));
    }
}
