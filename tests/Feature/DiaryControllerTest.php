<?php

namespace Tests\Feature;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Tests\TestHelper;

class DiaryControllerTest extends TestCase
{
    use RefreshDatabase;

    private $token;
    private $fs;


    public function setUp(): void
    {
        parent::setUp();
        $admin = TestHelper::getAdmin();
        $token = TestHelper::createToken($admin);

        $this->token = $token;
    }

    public function testDiaryCreate(): void
    {
        $request = [
            'date' => '2024-01-11 16:15:33Z',
            'title' => 'Prechadzka',
            'keywords' => 'prechadzka'
        ];

        $response = $this->withToken($this->token)->postJson('/api/admin/diaries', $request);

        $original = $response->original;

        $this->assertEquals($original->title, 'Prechadzka');
        $this->assertEquals($original->keywords, 'prechadzka');
        $this->assertEquals($original->tripTypes->count(), 0);
        $this->assertEquals($original->gpx->count(), 0);
        $this->assertEquals($original->photogallery->count(), 0);
        $this->assertInstanceOf(Carbon::class, $original->from_date);
        $this->assertDatabaseHas('diaries', [
            'title' => 'Prechadzka',
            'from_date' => '2024-01-11',
            'keywords' => 'prechadzka',
        ]);

    }

    public function testDiaryCreateWithPhotogallery()
    {
        copy('tests/diary/pictures.zip', 'tests/diary/pictures_copy.zip');

        $photosFile = new UploadedFile('tests/diary/pictures_copy.zip', 'pictures_copy.zip', test: true);

        $request = [
            'date' => '2024-03-15 16:15:33Z',
            'title' => 'Prechadzka2',
            'keywords' => 'prechadzka2',
            'photosFile' => $photosFile
        ];

        $response = $this->withToken($this->token)->postJson('/api/admin/diaries', $request);

        $original = $response->original;

        $this->assertEquals($original->photogallery->count(), 2);
        $this->assertEquals($original->uploadedFiles->count(), 2);
    }

    public function testDiaryCreateWithPhotogalleryAndLaterUpdate()
    {
        copy('tests/diary/pictures.zip', 'tests/diary/pictures_copy.zip');

        $photosFile = new UploadedFile('tests/diary/pictures_copy.zip', 'pictures_copy.zip', test: true);

        $request = [
            'date' => '2024-03-15 16:15:33Z',
            'title' => 'Prechadzka2',
            'keywords' => 'prechadzka2',
            'photosFile' => $photosFile
        ];

        $response = $this->withToken($this->token)->postJson('/api/admin/diaries', $request);

        $original = $response->original;

        $id = $original->id;

        $this->assertEquals($original->photogallery->count(), 2);
        $this->assertEquals($original->uploadedFiles->count(), 2);

        //update
        copy('tests/diary/photo2.zip', 'tests/diary/photo2_copy.zip');

        $photosFile2 = new UploadedFile('tests/diary/photo2_copy.zip', 'photo2_copy.zip', test: true);

        $request = [
            'photosFile' => $photosFile2
        ];

        $response = $this->withToken($this->token)->putJson(sprintf('/api/admin/diaries/%d', $id), $request);
        $original = $response->original;

        $this->assertEquals($original->photogallery->count(), 4);
    } 
}
