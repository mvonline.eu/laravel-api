<?php

namespace Tests\Feature;

use App\Models\MailingList;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Tests\TestCase;

class MailingListTest extends TestCase
{


    public function testAddToMailingList(): void
    {

        $payload = [
            'email' => 'jozko@be.sk'
        ];

        $response = $this->json('post', 'api/mailing', $payload);

        $response->assertStatus(200);
    }

    public function testAddSameEmail(): void
    {

        $payload = [
            'email' => 'jozko@be.sk'
        ];

        $response = $this->json('post', 'api/mailing', $payload);

        $response->assertStatus(409);
    }

    public function testRemoveEmailSuccessfull(): void
    {

        $mailing = MailingList::where('email', 'jozko@be.sk')->first();

        $hash = $mailing->hash;

        $response = $this->json('get', "api/mailing/{$hash}/remove");

        $response->assertStatus(302);

        $this->assertDatabaseHas('mailing_lists', [
            'email' => 'jozko@be.sk',
            'removed' => 1,
            'hash' => null
        ]);
    }

    public function testReinitializeEmail(): void
    {
        $this->testAddToMailingList();

        $this->assertDatabaseHas('mailing_lists', [
            'email' => 'jozko@be.sk',
            'removed' => 1,
        ]);

        $this->assertDatabaseHas('mailing_lists', [
            'email' => 'jozko@be.sk',
            'removed' => 0,
        ]);

        $this->assertDatabaseCount('mailing_lists', 2);
    }

    public function testFalseHashRemove(): void
    {
        $response = $this->json('get', "api/mailing/4444444/remove");
        $response->assertRedirect('/');
    }
}
