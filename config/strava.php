<?php

return [
    'client_id' => env('STRAVA_CLIENT_ID', ''),
    'client_secret' => env('STRAVA_CLIENT_SECRET', ''),
    'refresh_token' => env('STRAVA_REFRESH_TOKEN', ''),
];
