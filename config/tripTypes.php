<?php

return [
    1 => [
        'name' => 'Turistika',
        'icon' => 'fas fa-hiking',
    ],
    2 => [
        'name' => 'Chôdza',
        'icon' => 'fas fa-walking',
    ],
    3 => [
        'name' => 'Beh',
        'icon' => 'fas fa-running',
    ],
    4 => [
        'name' => 'Auto',
        'icon' => 'fas fa-car',
    ],
    5 => [
        'name' => 'Verejná doprava',
        'icon' => 'fas fa-bus',
    ],
    6 => [
        'name' => 'Bicykel',
        'icon' => 'fas fa-biking',
    ],
    7 => [
        'name' => 'Klasické lyžovanie',
        'icon' => 'fas fa-skiing-nordic',
    ],
];
