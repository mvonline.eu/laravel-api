<?php

use App\Http\Controllers\DiariesController;
use App\Http\Controllers\GeocachingController;
use App\Http\Controllers\MailingController;
use App\Http\Middleware\AdminAccess;
use App\Http\Middleware\ForceJSONResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::group([
    'prefix' => 'diaries'
], function (){
    Route::get('/list',  [DiariesController::class, 'list']);
    Route::get('/search',  [DiariesController::class, 'search']);
    Route::get('/{id}',  [DiariesController::class, 'detail']);
    Route::get('/{id}/geodata',  [DiariesController::class, 'geoData']);
});

Route::post('mailing', [MailingController::class, 'add']);
Route::get('mailing/{hash}/remove', [MailingController::class, 'delete'])->name('remove_from_mailing');
