<?php

use App\Admin\Controllers\DashboardController;
use App\Admin\Controllers\LoginController;
use App\Admin\Middleware\AdminAuthenticatedMiddleware;
use App\Admin\Controllers\DiariesController;
use Illuminate\Support\Facades\Route;

Route::get('/mailing/removed', function () {
    return view('mailingList.removed');
});

Route::get('/admin/login',[LoginController::class, 'index'])->name('admin.login');
Route::post('/admin/login',[LoginController::class, 'authenticate']);

Route::group([
    'namespace' => 'App\Admin\Controllers',
    'middleware' => AdminAuthenticatedMiddleware::class,
    'prefix' => 'admin',
], function () {
    Route::get('/', [DashboardController::class, 'index'])->name('admin.index');
    Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('admin.dashboard');
    Route::group(['prefix' => 'diaries'], function () {
        Route::get('/', [DiariesController::class, 'list'])->name('admin.diaries');
        Route::get('/add', [DiariesController::class, 'add'])->name('admin.diaries.add');
        Route::post('/post', [DiariesController::class, 'postDiary'])->name('admin.diaries.postDiary');
        Route::get('/edit/{id}', [DiariesController::class, 'edit'])->name('admin.diaries.edit');
    });
});
