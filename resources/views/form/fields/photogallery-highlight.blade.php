<div class="form-group">
    <label>{{ $options['label'] }}</label>
    <ul class="flex photogallery-highlight">
        <div class="grid gap-2 grid-cols-5">
            @foreach ($options['choices'] as $key => $choice)
                <li class="list-none m-0 float-left border border-1 border-transparent">
                    <input
                        class="hidden invisible w-[0px] h-0 relative"
                        type="radio"
                        name="{{ $name }}"
                        value="{{ $key }}"
                        id="{{ $name }}_{{ $key }}"
                        class="form-check-input"
                        {{ in_array($key, (array) $options['value']) ? 'checked' : '' }}
                        {{ $options['attributes'] ?? ''}}
                    />
                    <label for="{{ $name }}_{{ $key }}" class="relative block">
                        <span class="absolute z-10"></span>
                        <img class="relative shadow-mv cursor-pointer" src="{{ $choice['info']['thumb'] }}" alt="">
                    </label>
                </li>
            @endforeach
        </div>
    </ul>
    @if ($errors->has($name))
        <span class="invalid-feedback d-block">{{ $errors->first($name) }}</span>
    @endif
</div>
