<div class="form-group">
    <label>{{ $options['label'] }}</label>
    <ul class="tripTypes-choice">
        @foreach ($options['choices'] as $key => $choice)
            <li class="relative flex">
                <input
                        type="checkbox"
                        name="{{ $name }}[]"
                        value="{{ $key }}"
                        id="{{ $name }}_{{ $key }}"
                        class="form-check-input"
                        {{ in_array($key, (array) $options['value']) ? 'checked' : '' }}
                        {{ $options['attributes'] ?? ''}}
                />
                <label for="{{ $name }}_{{ $key }}" class="form-check-label items-center justify-center w-full h-full cursor-pointer">
                    <i class="{{$choice['icon']}}"></i>
                </label>
            </li>
        @endforeach
    </ul>
    @if ($errors->has($name))
        <span class="invalid-feedback d-block">{{ $errors->first($name) }}</span>
    @endif
</div>
