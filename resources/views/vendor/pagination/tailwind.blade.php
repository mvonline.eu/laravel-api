@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="my-4 flex justify-center">
        <div class="flex text-sm border border-gray-300 inline-block overflow-hidden">
            <div class="w-full flex justify-center items-center">
                <div class="w-full flex justify-center items-center">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span class="p-4 border-r border-gray-300 text-gray-400" aria-hidden="true">
                            <i class="fa fa-arrow-left"></i>
                        </span>
                    @else
                        <a href="{{ $paginator->previousPageUrl() }}" rel="prev"
                           class="p-4 border-r border-gray-300 hover:bg-mvonline hover:text-white"
                           aria-label="{{ __('pagination.previous') }}">
                            <i class="fa fa-arrow-left"></i>
                        </a>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <span class="p-4 border-r border-gray-300">{{ $element }}</span>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <span class="p-4 border-r border-gray-300 bg-mvonline text-white">{{ $page }}</span>
                                @else
                                    <a class="p-4 border-r border-gray-300 hover:bg-mvonline hover:text-white"
                                       href="{{ $url }}" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                        {{ $page }}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <a class="p-4 border-gray-300 hover:bg-mvonline hover:text-white" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="{{ __('pagination.next') }}">
                            <i class="fa fa-arrow-right"></i>
                        </a>
                    @else
                        <span class="p-4 border-gray-300 text-gray-300" aria-hidden="true">
                            <i class="fa fa-arrow-right"></i>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </nav>
@endif
