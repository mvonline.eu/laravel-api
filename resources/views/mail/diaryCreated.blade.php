 <html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Nový výlet</title>
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    </head>
    <body style="font-family: 'Roboto', sans-serif">
        <table>
            <tr>
                <td>
                    <img src="http://mvonline.eu/logo.png"/>
                </td>
            </tr>
            <tr>
                <td style="margin-top:50px">
                    <h1>Nový výlet</h1>
                    <div style="margin-top: 25px">
                        Na mojej stránke <a href="{{$url}}">{{$page}}</a> pribudol nový výlet. Tvojou výhodou ako odberateľa je, že na ňu nemusíš hneď chodiť, ale prekliknúť sa rovno na výlet, a vieš o ňom medzi prvými.
                    </div>
                    <div style="margin-top: 10px">
                        <h2>Tu je výlet:</h2>
                        <div>
                            <a href="{{$frontend.'/diary/'.$diaryId.'-'.$slug}}">
                                <figure style="position: relative; margin:0; width:222px; height:148px; overflow:hidden">
                                    <img style="object-fit: cover" src="{{$image ? $image['thumb'] : ""}}">
                                    <figcaption style="text-align: center; position: absolute; z-index:10; background-color: rgb(0 0 0 / 0.4); bottom: 0px; color: #fff">
                                        {{$title}}
                                    </figcaption>
                                </figure>
                            </a>
                        </div>
                    </div>  
                </td>
            </tr>
            <tr>
                <td style="font-size: 15px; color: silver; margin-top:20px">
                    Tento e-mail ti prišiel preto, že si prihlásený na ober výletov na stránke mvonline.eu. <a href="{{$hash}}">Kliknutím sem</a> zrušíš odber výletov.    
                </td>
            </tr>
        </table>
    </body>
</html>