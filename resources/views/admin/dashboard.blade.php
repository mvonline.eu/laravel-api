@extends('admin/layout')
@section('title', 'Dashboard | Administrácia')

@section('content')
    <div class="mv-container">
        <div class="grid grid-cols-4 gap-4">
            @foreach($links as $link)
                <a class="bg-white hover:bg-mvonline hover:text-white" href="{{ route($link['route']) }}">
                    <div class="border shadow text-center">
                        <b> {{$link['name']}} </b>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
