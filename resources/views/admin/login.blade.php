@dump($errors)
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Administrácia|MvOnline.eu
    </title>
    @vite('resources/css/app.css')
</head>
<body>
<div class="flex min-h-full flex-col justify-center px-6 py-12 lg:px-8 bg-slate-100">

    <div class="sm:mx-auto sm:w-full sm:max-w-sm p-5 border shadow bg-white">
        <img class="mx-auto h-10 w-auto" src="/images/logo.png" alt="MvOnline">
        <h2 class="mt-10 text-center text-2xl/9 font-bold tracking-tight text-gray-900">Vstup do administrácie</h2>

        <div class="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
            @if($errors->any())
                <div
                    class="bg-red-500 border border-gray-300 text-gray-900 sm:text-sm p-2.5">{{ $errors->first() }}</div>
            @endif
            <form class="space-y-6" method="POST">
                @csrf
                <div>
                    <label for="email" class="block text-sm/6 font-medium text-gray-900">Email</label>
                    <div class="mt-2">
                        <input id="email" name="email" type="email" autocomplete="email" required
                               class="block w-full border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm/6">
                    </div>
                </div>

                <div>
                    <div class="flex items-center justify-between">
                        <label for="password" class="block text-sm/6 font-medium text-gray-900">Heslo</label>
                    </div>
                    <div class="mt-2">
                        <input id="password" name="password" type="password" autocomplete="current-password" required
                               class="block w-full border-0 px-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm/6">
                    </div>
                </div>

                <div>
                    <button type="submit"
                            class="flex w-full justify-center bg-mvonline px-3 py-1.5 text-sm/6 font-semibold text-white shadow-sm hover:bg-mvonline focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600indigo-600 cursor-pointer">
                        Prihlásenie
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
