@extends('admin.layout')

@section('content')
    <div class="mv-container">

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

        <div class="w-full flex ml-0 mb-1">
            <a href="{{ route('admin.diaries.add') }}"
               class="px-4 py-2 bg-mvonline border border-gray-300 ml-auto hover:bg-mvonline-lighter text-white">Pridať
                výlet</a>
        </div>

        <table class="mv-table table-auto w-full border">
            <thead>
            <tr class="border bg-mvonline text-gray-300">
                <th class="p-2">ID</th>
                <th class="p-2">Title</th>
                <th class="p-2">Date</th>
                <th class="p-2">Akcie</th>
            </tr>
            </thead>
            <tbody>
            @foreach($diaries as $diary)
                <tr class="cursor-pointer">
                    <td class="border p-2 text-center">
                        {{ $diary->id }}
                    </td>
                    <td class="border p-2 text-left">
                        {{ $diary->title }}
                    </td>
                    <td class="border p-2 text-center">
                        {{ $diary->getFormattedDate()}}
                    </td>
                    <td class="border p-2 text-center">
                        <a href="{{ route('admin.diaries.edit', ['id' => $diary->id]) }}">
                            <i class="fa fa-edit"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $diaries->links() }}
    </div>
@endsection
