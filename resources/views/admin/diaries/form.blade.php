@extends('admin.layout')

@section('content')
    <div class="mv-container">
        <div class="w-full border border-mvonline clear-both">
            {!! form($form) !!}
        </div>
    </div>
@endsection
