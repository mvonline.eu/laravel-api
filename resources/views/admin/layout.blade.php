<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    @vite('resources/css/app.css')
    @vite('resources/assets/fontawesome/css/all.css')
    @yield('head')
</head>
<body>
<div id="root">
    <div class="app w-full h-full bg-slate-100">
        @include('admin/banner')

        @yield('content')
    </div>
</div>
@yield('script')
</body>
</html>
