<div class="w-full flex h-[100px] bg-white border border-t-0 border-l-0 border-r-0 border-b-2">
    <div class="mv-header flex">
        <div class="self-center"><a href="/admin/dashboard"><img src="/images/logo.png" alt="MvOnline"></a></div>

        <div class="self-center h-full md:hidden float-right ml-auto items-center flex">
            <div>
                <div class="cursor-pointer">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bars"
                         class="svg-inline--fa fa-bars fa-2x " role="img" xmlns="http://www.w3.org/2000/svg"
                         viewBox="0 0 448 512">
                        <path fill="currentColor"
                              d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"></path>
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
