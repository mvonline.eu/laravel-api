@php
    use App\Models\Form\Form;
    use App\Models\Form\FormField;
    /** @var Form $form */
    /** @var FormField $field */
@endphp
@foreach($form->getFields() as $field)
    <div>
        <div class="my-1">
            <label
                class="flex font-bold text-sm text-gray-500">{{ $field->getLabel() ?? '' }}{{$field->isRequired() ? '*' : ''}}
            </label>
        </div>
    </div>
    <div>
        @if ($field->getType() === FormField::TYPE_TEXT)
            <input class="form-input"
                   placeholder=" {{ $field->getPlaceholder() ?? '' }}" name="{{ $field->getName() }}" type="text"
                   value="{{ $field->getCurrentValue() }}">
        @endif
        @if ($field->getType() === FormField::TYPE_DATE)
            <input type="date" class="form-input" placeholder=" {{ $field->getPlaceholder() ?? '' }}"
                   name="{{ $field->getName() }}"
                   value="{{ $field->getCurrentValue()->format('Y-m-d') }}">
        @endif
        @if ($field->getType() === FormField::TYPE_FILE)
            <input type="file" class="form-input" placeholder=" {{ $field->getPlaceholder() ?? '' }}"
                   name="{{ $field->getName() }}"
                   value="{{ $field->getCurrentValue() }}">
        @endif
        @if ($field->getView())
            {{ $field->getView() }}
        @endif
    </div>
    @if ($field->getErrorMessage())
        <div class="text-red-800 font-bold">{{ $field->getErrorMessage() }}</div>
    @endif
@endforeach
<input type="hidden" name="_form" value="{{$form->getTitle()}}"/>

{!! $form->renderFooter()  !!}
