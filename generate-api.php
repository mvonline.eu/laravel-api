<?php

require 'vendor/autoload.php';

use OpenApi\Generator;


// Define the paths to scan for OpenAPI annotations
$pathsToScan = [
    'app/Http/Controllers', // Laravel Controllers
    'app/Models',          // Optional: Include Models for Schema definitions
];

try {
    // Scan the specified paths for annotations
    $openapi = Generator::scan($pathsToScan);

    // Output the OpenAPI spec as JSON
    $outputFile = base_path('storage/openapi.json'); // Save the spec in storage folder
    file_put_contents($outputFile, $openapi->toJson(JSON_PRETTY_PRINT));

    echo "OpenAPI spec generated successfully: {$outputFile}\n";
} catch (Exception $e) {
    echo "Error generating OpenAPI spec: " . $e->getMessage() . "\n";
}
